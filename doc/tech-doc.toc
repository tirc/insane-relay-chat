\select@language {english}
\contentsline {section}{\numberline {1}Introduction}{2}
\contentsline {section}{\numberline {2}Core IRC Server}{2}
\contentsline {subsection}{\numberline {2.1}Connection and Request Handling}{2}
\contentsline {subsection}{\numberline {2.2}IRC Protocol}{3}
\contentsline {subsection}{\numberline {2.3}Additional Features}{3}
\contentsline {subsubsection}{\numberline {2.3.1}Settings File}{3}
\contentsline {subsubsection}{\numberline {2.3.2}Logging}{3}
\contentsline {section}{\numberline {3}Plugins System}{4}
\contentsline {subsection}{\numberline {3.1}Plugin Design}{4}
\contentsline {subsubsection}{\numberline {3.1.1}Commands}{4}
\contentsline {subsubsection}{\numberline {3.1.2}IRC Events}{4}
\contentsline {subsubsection}{\numberline {3.1.3}Plugin Descriptors}{4}
\contentsline {subsection}{\numberline {3.2}Plugin Loader}{5}
\contentsline {subsection}{\numberline {3.3}Persistent Storage}{5}
