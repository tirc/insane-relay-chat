\documentclass[a4paper]{article}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage[colorinlistoftodos]{todonotes}

\title{Insane Relay Chat: Technical Document \\ System Design and Implementation}
\author{David Futcher, Elliot Iddon}

\date{\today}

\begin{document}
\maketitle
\hfill
    \includegraphics[width=250px]{../res/insane-relay-chat.png}
\hspace*{\fill}
\pagebreak

\tableofcontents

\section{Introduction}
Insane Relay Chat is an IRC server designed with concurrency and extensibility in mind. The system is split primarily into the core server, which provides a mostly standards (RFC 2812) compliant IRC implementation, and a plugins system, which provides a platform to write modular extensions that interact with the IRC server.

In order to create a well-designed system, a prototype version of the IRC server was written. During the process a simplified but mostly complete implementation of the IRC protocol and a prototypical incarnation of the Plugins system were built, in order to identify what both systems needs (for example, which information needs to be available to a plugin to be useful) and any pitfalls in a naive design of those systems. Lessons learned in the prototype stage helped shape the design in the result final development stage.

\section{Core IRC Server}
% Intro? 

\subsection{Connection and Request Handling}
The main Server class runs as a thread and listens for connecting clients on it's listening socket. As soon as a connection is received, a second thread, RequestHandler, is created and run, which handles only that specific connection, leaving the Server class to listen for more clients without being blocked. The Server class uses a ThreadPool (specifically a caching thread pool) to create these RequestHandler threads in a more efficient manner.

Once the RequestHandler thread for a connection has been started, it continually reads from the client socket and process the client's commands, until the socket is closed (either by the client itself closing it or the server closing it after receiving a QUIT command). 

\subsection{IRC Protocol}\label{sec:proto}
A conscious design decision was made to separate, where possible, the nuts and bolts of sending and receiving messages and the business logic of the IRC protocol itself. The RequestHandler reads the bytes representing a protocol command and it's parameters, then packages this into a higher-level Message class, before working out exactly which command had been sent and called the correct method on the IRCProtocol class. This IRCProtocol class represents a higher layer (almost analogous to the OSI network layer model) which only concerned itself with the higher-level logic of the IRC protocol.

The protocol class has a number of methods, each of which is responsible for the handling of a single protocol command. Each of these methods generally processes the parameters of the command, checking they are valid, then tells the Server class to perform an action and generates a response message. As each IRCProtocol instance is specific to a single client, the Server class is used to store information like the list of all current clients and channels and other similar global information. This server class is also responsible for passing relevant information to the currently loaded plugins and dealing with the responses from these plugins (more information in section \ref{sec:plugin-if}).

There are a number of other higher-level classes used by the IRCProtocol and Server classes to implement the protocol logic. Each client has a Client class, which represents an IRC user, holding data like their nickname, the channels they're currently in and their IRC modes. The concept of channels is represented by the Channel class, which stores (among other things) the channel's name and the list of Clients currently in the channel.

Design decisions relating to this part of the system include using the Factory pattern with MessageFactory, which provides a standardised and semi-automated way to generate response messages. Constant variables were also heavily used (for example in IRCNumerics and IRCReservedWords) in place of inline string/int literals. Both of these decisions were made after identifying issues with the code produced in the prototype stage of development.


\subsection{Additional Features}

\subsubsection{Settings File}\label{sec:settings}
A settings file is used to store key configuration values for the IRC server. It is stored in the JSON format and parsed using Google's GSON library. The file is primarily used to store key values for the Plugin Loader (see section \ref{sec:plugin-load}) -such as the directory plugins are stored and optionally the order in which specific plugins should be loaded - and the Persistent Storage (see section \ref{sec:db}) database connection settings. The settings are loaded when the server starts and are used for the life of the server. The settings are loaded into a Settings singleton to allow access from anywhere in the process.


\subsubsection{Logging}
Logging is used throughout Insane Relay Chat, used for both logging server events (as is common with servers) and debug/tracing events useful during development. A logging library (Minlog) was used to provide a low-overhead logging system. This library allows debug-level logging events to be ignored at build time, producing more efficient code when only logging server events. Several of the example plugins also use this logging library to log events.


\section{Plugins System}

The plugins system is designed to provide a modular way to extend the IRC system. On most IRC networks, it is common for so-called "IRC bots" to provide services such as nickname authentication ("NickServ") and channel management ("ChanServ"). These bots are implemented as IRC clients, which must implement at least the key parts of the (relatively complicated) IRC protocol. The idea driving the plugins system was that these services (and many others) could be implemented as plugins, without requiring any knowledge of the IRC specification.

\subsection{Plugin Design}\label{sec:plugin-if}
Plugins are defined in the Java Interface named Plugin. Many simple utility plugins like date don't need to utilise much of the functionailty provided by what is quite a broad interface, these may choose to instead extend AbstractPlugin. These plugins can be packaged into jars and then loaded by the PluginLoader.

\subsubsection{Commands}\label{sec:commands}
Plugins can register commands - specific strings (in this server beginning with an exclamation mark) which the server looks for in messages. When a command message is detected, the server calls the \textit{response()} method on the plugin that registered that command. This method takes an InternalIRCMessage instance, which stores the Message triggering the command and the Client that sent the Message (see \ref{sec:proto}). It returns a List of InternalMessages, an abstract class that is inherited by InternalIRCMessage, which is used to send a reply message to a user or a channel, and InternalCommandMessage, which gives an internal command to the server for example, create a channel or modify a user's mode.

\subsubsection{IRC Events}
Plugins have the opportunity to respond to events generated by the server e.g. a user joining the channel. When an event occurs each plugin has the oppurtunity to respond to the event via the \textit{handleEvent()} method from Plugin. This returns a List of InternalMessages in the same style of response as discussed in \ref{sec:commands}

\subsubsection{Plugin Descriptors}
The PluginDescriptor is a class used to describe plugins. It is retrieved from the Plugin instance via \textit{getPluginDescriptor()} this contains:
    \begin{enumerate}
        \item A port number which may be either the port that other plugins should use to communicate with it or -1 if no incoming communication is supported.
        \item A String which is the plugin's name.
        \item A String which is the plugin's version to allow other plugins to depend on a minimum version.
        \item A List of Strings which are the commands the plugin responds to.
        \item A List of Strings which are the help messages which should be displayed if requested.
    \end{enumerate}
Each new plugin that is loaded receives the PluginDescriptors of all previously loaded plugins via \textit{init()} to give it the opportunity to configure itself.

\subsection{Plugin Loader}\label{sec:plugin-load}
The PluginLoader performs a search of the directory specified by the Settings singleton see \ref{sec:settings} and attempts to instantiate and instance of the Plugin interface from the classes loaded from the jar. If there are no conflicts with existing plugin commands the plugin is loaded and registered as the response handler for its commands.
\subsection{Persistent Storage}\label{sec:db}
Plugins can access a per plugin table based key-value storage via the PersistentStorage interface. The current version of Insane Relay Chat only has one implementation of this interface which offers remote access to a MongoDB instance. The purpose of the interface is to abstract the storage concepts from implementation to allow new storage handlers to be added into the server modularly without changing any of the existing plugin code.
\end{document}
