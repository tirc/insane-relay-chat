package textadventure;

public enum Enemy {
	GOBLIN("gnarly goblin", 20, 10),
	DRAGON("dangerous, firebreathing dragon", 100, 40),
	OGRE("oblong ogre", 50, 10);
	
	private final String description;
	private final int hp;
	private final int baseDamge;
	
	private Enemy(String description, int hp, int baseDamage) {
		this.description = description;
		this.hp = hp;
		this.baseDamge = baseDamage;
	}
	
	public static Enemy forInt(int num){
		int eint = num % 3;
		switch(eint) {
		case 0:
			return GOBLIN;
		case 1:
			return DRAGON;
		case 2:
			return OGRE;
		}
		
		return GOBLIN;
	}

	public int hp() {
		return hp;
	}
	
	public int baseDamage(){
		return baseDamge;
	}
	
	public String getDescription(){
		return description;
	}
}
