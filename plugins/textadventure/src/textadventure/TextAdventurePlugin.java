package textadventure;

import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;
import ircserver.Message;
import ircserver.plugin.AbstractPlugin;
import ircserver.plugin.InternalMessageFactory;
import ircserver.plugin.InternalMessageFactoryFactory;
import ircserver.plugin.PersistentStorage;
import ircserver.plugin.PluginDescriptor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class TextAdventurePlugin extends AbstractPlugin {

	public TextAdventurePlugin(){
		super(NAME, VERSION, COMMANDS, HELP);
	}

	private static final String NAME = "textadventure";
	private static final String VERSION = "1.0";
	
	private static final String KEY_PLAYER = "player";
	
	private static final String COMMAND_MOVE = "ta-move";
	private static final String COMMAND_FIGHT = "ta-fight";
	private static final String COMMAND_LOOK = "ta-look";
	private static final String COMMAND_RESET = "ta-reset";
	
	private static final String RESPONSE_AUTH_UNAVAILABLE = "nickauth was unavailable. If it's loaded try again.";
	private static final String RESPONSE_NOT_AUTHED = "You're not authed. Talk to nickauth and then come back.";
	private static final String RESPONSE_MALFORMED = "See !help " + NAME;
	private static final String RESPONSE_RESET = "Per your request your gamesave has been deleted.";
	
	private static final List<String> COMMANDS = Arrays.asList(
			COMMAND_MOVE,
			COMMAND_FIGHT,
			COMMAND_LOOK,
			COMMAND_RESET
			);
	private static final List<String> HELP = Arrays.asList(
			"ta-move  < north | east | south | west > :: Move in the direction specified.",
			"ta-look :: Have your surroundings described to you.",
			"ta-fight :: Attack the enemy in front of you.",
			"ta-reset :: Start a new game."
			);
	
	
	
	private Socket nickAuth;
	private Socket serverCurrency;
	private PersistentStorage storage;
	private InternalMessageFactoryFactory factory;
	private Gson gson;
	
	@Override
	public void init(PersistentStorage storage, PluginDescriptor... ps) {
		int authPort = -1;
		int currencyPort = -1;
		
		for(PluginDescriptor pluginDescriptor : ps) {
			if(pluginDescriptor.pluginName.equals("nickauth")) {
				authPort = pluginDescriptor.portNumber;
			} else if (pluginDescriptor.pluginName.equals("servercurrency")) {
				currencyPort = pluginDescriptor.portNumber;
			}
		}
		
		if (authPort == -1) {
			nickAuth = null;
		} else {
			try {
				nickAuth = new Socket(InetAddress.getLocalHost(), authPort);
			} catch (IOException e) {
				if (nickAuth != null) {
					try {
						nickAuth.close();
					} catch (IOException ignored) {}
				}
				nickAuth = null;
			}
		}
		
		if (currencyPort == -1) {
			serverCurrency = null;
		} else {
			try {
				serverCurrency = new Socket(InetAddress.getLocalHost(), currencyPort);
			} catch (IOException e) {
				if (serverCurrency != null) {
					try {
						serverCurrency.close();
					} catch (IOException ignored) {}
				}
				serverCurrency = null;
			}
		}

		this.storage = storage;
		factory = new InternalMessageFactoryFactory(NAME);
		gson = new Gson();
	}
	
	@Override
	public List<InternalMessage> response(InternalIRCMessage m) {
		List<InternalMessage> responses = new ArrayList<>();
		InternalMessageFactory factory = this.factory.build(m.getClient());
		
		if (nickAuth == null) {
			responses.add(factory.newPrivateMessage(RESPONSE_AUTH_UNAVAILABLE));
			return responses;
		}

		
		if (isAuthed(m.getClient().getNick())) {
			Message msg = m.getMessage();
			String command = msg.params[1].substring(2);
			Player player = playerFor(m.getClient().getNick());
			switch(command) {
			case COMMAND_MOVE:
				if(msg.params.length < 3) {
					responses.add(factory.newPrivateMessage(RESPONSE_MALFORMED));
					break;
				}
				String directionString = msg.params[2].toLowerCase();
				Direction direction = null;
				switch(directionString) {
				case "north": direction = Direction.NORTH; break;
				case "east": direction = Direction.EAST; break;
				case "south": direction = Direction.SOUTH; break;
				case "west": direction = Direction.WEST; break;
				}
				if(direction == null){
					responses.add(factory.newPrivateMessage(RESPONSE_MALFORMED));
				} else {
					responses.add(factory.newPrivateMessage(player.move(direction)));
				}
				break;
			case COMMAND_LOOK:
				responses.add(factory.newPrivateMessage(player.look()));
				break;
			case COMMAND_FIGHT:
				responses.add(factory.newPrivateMessage(player.fight()));
				break;
			case COMMAND_RESET:
				responses.add(factory.newPrivateMessage(RESPONSE_RESET));
				if (serverCurrency != null) {
					depositCash(m.getClient().getNick(), player.getScore());
					responses.add(factory.newPrivateMessage(String.format("%s has been added to your servercurrency account.", player.getScore())));
				}
				player = new Player();
				break;
			}
			savePlayer(player, m.getClient().getNick());
		} else {
			responses.add(factory.newPrivateMessage(RESPONSE_NOT_AUTHED));
		}
		
		
		return responses;
	}
	
	private void savePlayer(Player player, String nick) {
		String playerString = gson.toJson(player);
		storage.setValue(NAME, nick, KEY_PLAYER, playerString);
	}
	
	private Player playerFor(String nick) {
		String playerString = storage.getValue(NAME, nick, KEY_PLAYER);
		if (playerString.equals("")) {
			return new Player();
		}

		Player player = null;
		try {
			player = gson.fromJson(playerString, Player.class);
			if (player == null)
				return new Player();
		} catch (JsonSyntaxException e) {
			return new Player();
		}

		return player;
	}
	
	private void depositCash(String nick, int quantity) {
		BufferedReader reader = null;
		
		if (serverCurrency != null) {
			if (!serverCurrency.isClosed()) {
				try {
					serverCurrency.getOutputStream().write(String.format("DEPOSITMONEY %s %s\n", nick, quantity).getBytes());
				} catch (IOException ignored) {
					
				}
			}
		}
		
	}
	
	private boolean isAuthed(String nick){
		BufferedReader reader = null;
		
		if(!nickAuth.isClosed()) {
			try {
				nickAuth.getOutputStream().write(String.format("ISAUTHED %s\n", nick).getBytes());
			
				reader = new BufferedReader(new InputStreamReader(nickAuth.getInputStream()));
				String line = reader.readLine();
				
				return line.equals(String.format("AUTHED %s", nick));
			} catch (IOException ignored) {
				
			}
			
		}
		
		return false;
	}

}
