package textadventure;

public enum Field {

	PLAINS("grassy plains"),
	MOUNTAINS("rocky mountains"),
	FORREST("leafy forrest");
	
	private final String name;
	
	private Field(String name){
		this.name = name;
	}
	
	public String getName(){
		return name;
	}
	
}
