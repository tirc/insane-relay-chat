package textadventure;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

public class Player {

	private static final String BLOCKED_PATH = "An unscalable wall blocks your path.";
	private static final String YOU_STAND_IN_S = "You stand in a %s";
	private static final String YOU_FIND_POT = "You find a potion; it heals you 5hp";
	
	private static final int MAX_HP = 100;
	
	private Field[][] fields;
	private State state;
	private Enemy enemy;
	private int enemyHp;
	private int hp;
	private int x;
	private int y;
	private int score;
	
	private static Random r = new Random();
	
	public static void main(String... args){
		Player p = new Player();
		BufferedReader r = new BufferedReader(new InputStreamReader(System.in));
		String line;
		try {
			while((line = r.readLine()) != null) {
				switch(line) {
				case "look":
					System.out.println(p.look());
					break;
				case "fight":
					System.out.println(p.fight());
					break;
				case "moveN":
					System.out.println(p.move(Direction.NORTH));
					break;
				case "moveS":
					System.out.println(p.move(Direction.SOUTH));
					break;
				case "moveE":
					System.out.println(p.move(Direction.EAST));
					break;
				case "moveW":
					System.out.println(p.move(Direction.WEST));
					break;
				default:
					System.out.println(String.format("I don't know the command %s", line));
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Player(){
		x = 0;
		y = 0;
		fields = new Field[100][100];
		state = State.INIT;
		enemy = null;
		hp = MAX_HP;
		score = 0;
	}
	
	private Field getField(){
		if (fields[x][y] == null) {
			switch(r.nextInt(3)){
			case 0: fields[x][y] = Field.PLAINS; break;
			case 1: fields[x][y] = Field.MOUNTAINS; break;
			case 2: fields[x][y] = Field.FORREST; break;
			}
		}
		return fields[x][y];
	}

	public synchronized String fight(){
		switch(state){
		case FIGHT:
			int enemyDamage = r.nextInt(21) - 10 + enemy.baseDamage();
			int yourDamage = r.nextInt(11);
			enemyHp -= yourDamage;
			if(enemyHp <= 0){
				state = State.START;
				score += enemy.hp();
				return String.format("Your strike of %d has defeated the enemy! Looting it's corpse you find %d gold! If you have the servercurrency plugin on this server this will be deposited in your account at the end of the game.", yourDamage, enemy.hp());
			}

			hp -= enemyDamage;
			if (hp <= 0) {
				state = State.DEAD;
				return String.format("You strike the enemy for %d but it's reply does %d to you! You die.", yourDamage, enemyDamage);
			}

			return String.format("You strike the enemy for %d. It strikes you back for %d", yourDamage, enemyDamage);
		case INIT:
		case START:
		case DEAD:
		default:
			return "Nothing to fight. Try looking around.";
		}
	}
	
	
	public synchronized String look(){
		switch(state) {
		case INIT:
			state = State.START;
			return "Your grand adventure is about to unfold; " + String.format(YOU_STAND_IN_S, getField().getName());
		case FIGHT:
			return String.format("You're faced with a %s", enemy.getDescription());
		case START:
			return String.format(YOU_STAND_IN_S, getField().getName());
		case DEAD:
			return String.format("You're dead. Reset to try again.");
		}
		return "Unknown state.";
	}
	
	public int getScore(){
		return score;
	}
	
	
	public synchronized String move(Direction d){
		String look = "";
		switch(state) {
		case INIT:
			return "Try looking around first.";
		case FIGHT:
			return "No! There's no running from a fight!";
		case START:
			switch(d) {
			case NORTH:
				if (y < 99) {
					y++;
					break;
				} else {
					return BLOCKED_PATH;
				}
			case SOUTH:
				if (y > 0) {
					y--;
					break;
				} else {
					return BLOCKED_PATH;
				}
			case EAST:
				if (x < 99) {
					x++;
					break;
				} else {
					return BLOCKED_PATH;
				}
			case WEST:
				if (x > 0) {
					x--;
					break;
				} else {
					return BLOCKED_PATH;
				}
			}
			look = String.format(YOU_STAND_IN_S, getField().getName());
			
			int droll = r.nextInt(100);
			if (droll < 30) {
				look = String.format("%s, %s", look, YOU_FIND_POT);
				hp = Math.min(MAX_HP, hp+5);
			} else if (droll < 60) {
				enemy = Enemy.forInt(droll);
				enemyHp = enemy.hp();
				look = String.format("%s, you encounter a %s.", look, enemy.getDescription());
				state = State.FIGHT;
			}
			
			break;
		case DEAD:
			return "Your adventure has ended in death. Reset to try again.";
		}
		
		return look;
	}
	
	
	
}
