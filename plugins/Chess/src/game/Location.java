package game;

public class Location {

	private int vertical;
	private int horizontal;
	
	public Location(int v, int h){
		vertical = v;
		horizontal = h;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + horizontal;
		result = prime * result + vertical;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (horizontal != other.horizontal)
			return false;
		if (vertical != other.vertical)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Location [vertical=" + vertical + ", horizontal=" + horizontal
				+ "]";
	}

	public int getVertical(){
		return vertical;
	}
	
	public int getHorizontal(){
		return horizontal;
	}
}
