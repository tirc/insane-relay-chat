package game;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public abstract class ChessPiece {
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((color == null) ? 0 : color.hashCode());
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChessPiece other = (ChessPiece) obj;
		if (color != other.color)
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ChessPiece [type=" + type + ", location=" + location
				+ ", color=" + color + "]";
	}

	private Type type;
	public Location location;
	protected Color color;
	private ArrayList<ChessMove> previousMoves;
	
	public ChessPiece(Type t, Location l, Color c){
		type = t;
		location = l;
		color = c;
		previousMoves = new ArrayList<ChessMove>();
	}
	
	public void newMove(ChessMove move){
		previousMoves.add(move);
	}
	public ArrayList<ChessMove> getPreviousMoves(){
		return previousMoves;
	}
	
	public Type getType(){
		return type;
	}
	
	public void setLocation(Location l){
		location = l;
	}
	
	public Location getLocation(){
		return location;
	}
	
	public Color getColor(){
		return color;
	}
	
	public abstract Set<Location> nextStates(HashMap<Location,ChessPiece> black, HashMap<Location, ChessPiece> white);
}
