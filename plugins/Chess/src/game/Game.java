package game;

import ircserver.Client;

public class Game {

	Client user1;
	Client user2 = null;
	String tempu2 = "";
	Board game;
	boolean accepted = true;
	
	public Game(Client u1, String u2, Board g){
		user1 = u1;
		tempu2 = u2;
		game = g;
		accepted = false;
	}
	
	public Game(Client u1, Board g){
		user1 = u1;
		game = g;
	}
	
	public void acceptGame(Client u2){
		tempu2 = "";
		user2 = u2;
		accepted = true;
	}
	
	public Client getOpponent(Client u){
		if(u.equals(user1)){
			return user2;
		}
		else if(u.equals(user2)){
			return user1;
		}
		else{
			System.out.println("User requested that doesn't exist.");
			return null;
		}
	}
	
	public int getUser(Client u){
		if(user1.equals(u)){
			return 1;
		}
		if(user2.equals(u)){
			return 2;
		}
		return 0;
	}
	
	public boolean getWaitingUser(String s){
		if(tempu2.equals(s)){
			return true;
		}
		return false;
	}
	
	public Board getBoard(){
		return game;
	}
}
