package game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.esotericsoftware.minlog.Log;

import ircserver.Client;
import ircserver.IRCEvent;
import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;
import ircserver.Message;
import ircserver.plugin.AbstractPlugin;
import ircserver.plugin.InternalMessageFactory;
import ircserver.plugin.InternalMessageFactoryFactory;
import ircserver.plugin.PersistentStorage;
import ircserver.plugin.PluginDescriptor;

public class ChessPlugin extends AbstractPlugin{
	
	public static final String NAME = "chess";
	private static final String VERSION = "1.0";
	
	private static final String RESPONSE_MALFORMED = "See !help " + NAME;
	private static final String RESPONSE_GAME_IN_PROGRESS = "Only one game can be played at a time, game already in progress.";
	private static final String RESPONSE_GAME_ENDED = "Game ended by user: ";
	private static final String RESPONSE_NO_GAME = "No game associated with this user.";
	private static final String RESPONSE_NOT_VALID_LOCATION = "The location you entered is not a valid chess location see !help " + NAME;
	private static final String RESPONSE_GAME_ACCEPTED = "The game has been accepted, the requestee goes first.";
	private static final String RESPONSE_NOT_YOUR_TURN = "It's not your move.";
	
	private static final String NEW1PGAME = "new-1p-game";
	private static final String NEW2PGAME = "new-2p-game";
	private static final String MAKEMOVE = "make-move";
	private static final String QUITGAME = "quit-game";
	private static final String ACCEPTGAME = "accept-game";
	private static final String ALMOSTFINISHED = "almost-finished";
	
	private static final List<String> COMMANDS = Arrays.asList(
			NEW1PGAME,
			NEW2PGAME,
			MAKEMOVE,
			ACCEPTGAME,
			QUITGAME,
			ALMOSTFINISHED
			);
	
	private static final List<String> HELP = Arrays.asList(
			"new-1p-game :: start a new game game played on one machine.",
			"new-2p-game <opponents-nick> :: start a new game with the player specified",
			"make-move <current-location> <new-location> :: make a move e.g make-move e5 e7",
			"accept-game :: accept a game that another user has requested",
			"almost-finished :: put the board to one move away from finished, the move is e4 d6",
			"quit-game :: quit the current game"
			);
	
	private InternalMessageFactoryFactory factory;
	
	private List<Game> games;

	public ChessPlugin() {
		super(NAME, VERSION, COMMANDS, HELP);
		games = new ArrayList<Game>();
	}
	
	@Override
	public void init(PersistentStorage storage, PluginDescriptor... ps) {
		factory = new InternalMessageFactoryFactory(NAME);
	}
	

	@Override
	public List<InternalMessage> response(InternalIRCMessage m) {
		Log.trace("receives message.");
		List<InternalMessage> responses = new ArrayList<>();
		Message msg = m.getMessage();
		
		Client client = m.getClient();
		
		InternalMessageFactory factory = this.factory.build(client);
	
		if(msg.params.length < 2) {
			responses.add(factory.newPrivateMessage(RESPONSE_MALFORMED));
		} else {
			String command = msg.params[1].substring(2);
			Log.trace("command received.");
			switch(command){
				case NEW1PGAME:
					Log.trace("new 1p game");
					for(Game g : games){
						if(g.getUser(client) > 0){
							responses.add(factory.newPrivateMessage(RESPONSE_GAME_IN_PROGRESS));
							break;
						}
					}
					Board board = new Board();
					games.add(new Game(client, board));
					for(String str : board.printBoard()){
						responses.add(factory.newPrivateMessage(str));
					}
					break;
				case NEW2PGAME:
					Log.trace("new 2p game");
					if(msg.params.length < 3) {
						responses.add(factory.newPrivateMessage(RESPONSE_MALFORMED));
						break;
					}
					for(Game g : games){
						if(g.getUser(client) > 0){
							responses.add(factory.newPrivateMessage(RESPONSE_GAME_IN_PROGRESS));
							break;
						}
					}
					Board board2 = new Board();
					games.add(new Game(client, msg.params[2], board2));
					for(String str : board2.printBoard()){
						responses.add(factory.newPrivateMessage(str));
					}
					break;
				case MAKEMOVE:
					Log.trace("new move");
					InternalMessageFactory factory2 = null;
					if(msg.params.length < 4) {
						responses.add(factory.newPrivateMessage(RESPONSE_MALFORMED));
					}
					String horizontal = "abcdefgh";
					String vertical = "12345678";
					String oldLocation = msg.params[2];
					String newLocation = msg.params[3];
					if(!horizontal.contains(oldLocation.substring(0,1).toLowerCase())){
						responses.add(factory.newPrivateMessage(RESPONSE_NOT_VALID_LOCATION));
					}
					if(!vertical.contains(oldLocation.substring(1,2))){
						responses.add(factory.newPrivateMessage(RESPONSE_NOT_VALID_LOCATION));
					}
					if(!horizontal.contains(newLocation.substring(0,1).toLowerCase())){
						responses.add(factory.newPrivateMessage(RESPONSE_NOT_VALID_LOCATION));
					}
					if(!vertical.contains(newLocation.substring(1,2))){
						responses.add(factory.newPrivateMessage(RESPONSE_NOT_VALID_LOCATION));
					}
					for(Game g : games){
						int move = g.getUser(client);
						Client u2 = g.getOpponent(client);
						if(move == 1 && g.getBoard().getCurrentTurn() == Color.WHITE){
							Location oldL = new Location(vertical.indexOf(oldLocation.substring(1,2)), horizontal.indexOf(oldLocation.substring(0,1))); 
							Location newL = new Location(vertical.indexOf(newLocation.substring(1,2)), horizontal.indexOf(newLocation.substring(0,1)));
							for(String s : g.getBoard().makeMove(oldL, newL)){
								responses.add(factory.newPrivateMessage(s));
								if(u2 != null){
									if(u2.equals(client)){
										Log.trace("Same user");
									} else {
										Log.trace("Different user");
									}
									factory2 = this.factory.build(u2);
									factory2.setClient(u2);
									responses.add(factory2.newPrivateMessage(s));
								}
							}
							break;
						}
						else if(u2 == null || move == 2 && g.getBoard().getCurrentTurn() == Color.BLACK){
							Location oldL = new Location(vertical.indexOf(oldLocation.substring(1,2)), horizontal.indexOf(oldLocation.substring(0,1))); 
							Location newL = new Location(vertical.indexOf(newLocation.substring(1,2)), horizontal.indexOf(newLocation.substring(0,1)));
							for(String s : g.getBoard().makeMove(oldL, newL)){
								responses.add(factory.newPrivateMessage(s));
								if(u2 != null){
									factory2 = this.factory.build(u2);
									factory2.setClient(u2);
									responses.add(factory2.newPrivateMessage(s));
								}
							}
							break;
						}
						else if(move > 0){
							Log.trace(Integer.toString(move));
							responses.add(factory.newPrivateMessage(RESPONSE_NOT_YOUR_TURN));
							break;
						}
						else{
							responses.add(factory.newPrivateMessage(RESPONSE_NO_GAME));
							break;
						}
					}
					break;
				case ACCEPTGAME:
					Log.trace("game accepted");
					for(Game g : games){
						if(g.getWaitingUser(client.getNick())){
							g.acceptGame(client);
							Client opponent = g.getOpponent(client);
							InternalMessageFactory factoryOther = this.factory.build(opponent);
							responses.add(factory.newPrivateMessage(String.format(RESPONSE_GAME_ACCEPTED)));
							responses.add(factoryOther.newPrivateMessage(String.format(RESPONSE_GAME_ACCEPTED)));
							break;
						}
					}
					break;
				case QUITGAME:
					Log.trace("game quit");
					for(Game g : games){
						if(g.getUser(client) > 0){
							Client u2 = g.getOpponent(client);
							if(u2 != null){
								InternalMessageFactory factoryOther = this.factory.build(u2);
								responses.add(factoryOther.newPrivateMessage(String.format(RESPONSE_GAME_ACCEPTED)));
							}
							//TODO message both users
							responses.add(factory.newPrivateMessage(String.format(RESPONSE_GAME_ENDED,client.getNick())));
							games.remove(g);
							break;
						}
					}
					responses.add(factory.newPrivateMessage(RESPONSE_NO_GAME));
					break;
				case ALMOSTFINISHED:
					for(Game g : games){
						if(g.getUser(client) > 0){
							board = g.getBoard();
							board.newGame();
							board.makeMove(new Location(1,4), new Location(3,4));
							board.makeMove(new Location(6,2), new Location(5,2));
							board.makeMove(new Location(0,1), new Location(2,2));
							board.makeMove(new Location(6,3), new Location(4,3));
							board.makeMove(new Location(0,6), new Location(2,5));
							board.makeMove(new Location(4,3), new Location(3,4));
							board.makeMove(new Location(2,2), new Location(3,4));
							board.makeMove(new Location(7,6), new Location(5,5));
							board.makeMove(new Location(0,3), new Location(1,4));
							board.makeMove(new Location(7,1), new Location(6,3));
						}
						for(String s : g.getBoard().printBoard()){
							responses.add(factory.newPrivateMessage(s));
						}
					}
					break;
				default:
					Log.trace("no command");
			}
		}
		return responses;
	}
}
