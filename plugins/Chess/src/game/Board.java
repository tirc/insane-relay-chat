package game;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.esotericsoftware.minlog.Log;


public class Board {
	
	private ChessPiece[][] pieces;
	private HashMap<Location, ChessPiece> black;
	private HashMap<Location, ChessPiece> white;
	private ChessPiece blackKing;
	private ChessPiece whiteKing;
	private Color currentTurn = Color.WHITE;
	private boolean game;
	
	public Board(){
		newGame();
	}
	
	public void newGame(){
		pieces = new ChessPiece[8][8];
		black = new HashMap<Location, ChessPiece>();
		white = new HashMap<Location, ChessPiece>();
		ChessPiece blackPiece;
		ChessPiece whitePiece;
		for(int i=0;i<8;i++){
			blackPiece = new Pawn(Type.PAWN, new Location(6,i), Color.BLACK);
			whitePiece = new Pawn(Type.PAWN, new Location(1,i), Color.WHITE);
			black.put(blackPiece.getLocation(), blackPiece);
			white.put(whitePiece.getLocation(), whitePiece);
			pieces[6][i] = blackPiece;
			pieces[1][i] = whitePiece;
		}
		blackPiece = new Rook(Type.ROOK, new Location(7,0), Color.BLACK);
		black.put(blackPiece.getLocation(), blackPiece);
		pieces[7][0] = blackPiece;
		blackPiece = new Rook(Type.ROOK, new Location(7,7), Color.BLACK);
		black.put(blackPiece.getLocation(), blackPiece);
		pieces[7][7] = blackPiece;
		blackPiece = new Knight(Type.KNIGHT, new Location(7,1), Color.BLACK);
		black.put(blackPiece.getLocation(), blackPiece);
		pieces[7][1] = blackPiece;
		blackPiece = new Knight(Type.KNIGHT, new Location(7,6), Color.BLACK);
		black.put(blackPiece.getLocation(), blackPiece);
		pieces[7][6] = blackPiece;
		blackPiece = new Bishop(Type.BISHOP, new Location(7,2), Color.BLACK);
		black.put(blackPiece.getLocation(), blackPiece);
		pieces[7][2] = blackPiece;
		blackPiece = new Bishop(Type.BISHOP, new Location(7,5), Color.BLACK);
		black.put(blackPiece.getLocation(), blackPiece);
		pieces[7][5] = blackPiece;
		blackPiece = new Queen(Type.QUEEN, new Location(7,3), Color.BLACK);
		black.put(blackPiece.getLocation(), blackPiece);
		pieces[7][3] = blackPiece;
		blackKing = new King(Type.KING, new Location(7,4), Color.BLACK);
		black.put(blackKing.getLocation(), blackKing);
		pieces[7][4] = blackKing;
		
		whitePiece = new Rook(Type.ROOK, new Location(0,0), Color.WHITE);
		white.put(whitePiece.getLocation(), whitePiece);
		pieces[0][0] = whitePiece;
		whitePiece = new Rook(Type.ROOK, new Location(0,7), Color.WHITE);
		white.put(whitePiece.getLocation(), whitePiece);
		pieces[0][7] = whitePiece;
		whitePiece = new Knight(Type.KNIGHT, new Location(0,1), Color.WHITE);
		white.put(whitePiece.getLocation(), whitePiece);
		pieces[0][1] = whitePiece;
		whitePiece = new Knight(Type.KNIGHT, new Location(0,6), Color.WHITE);
		white.put(whitePiece.getLocation(), whitePiece);
		pieces[0][6] = whitePiece;
		whitePiece = new Bishop(Type.BISHOP, new Location(0,2), Color.WHITE);
		white.put(whitePiece.getLocation(), whitePiece);
		pieces[0][2] = whitePiece;
		whitePiece = new Bishop(Type.BISHOP, new Location(0,5), Color.WHITE);
		white.put(whitePiece.getLocation(), whitePiece);
		pieces[0][5] = whitePiece;
		whitePiece = new Queen(Type.QUEEN, new Location(0,3), Color.WHITE);
		white.put(whitePiece.getLocation(), whitePiece);
		pieces[0][3] = whitePiece;
		whiteKing = new King(Type.KING, new Location(0,4), Color.WHITE);
		white.put(whiteKing.getLocation(), whiteKing);
		pieces[0][4] = whiteKing;
		
		game = true;
		printBoard();
	}
	
	public boolean gameOver(){
		return game;
	}
	
	public List<String> printBoard(){
		List<String> response = new ArrayList<String>();
		System.out.println("  A B C D E F G H");
		response.add("  A B C D E F G H");
		for(int i = 7;i>=0;i--){
			String text = "";
			text += Integer.toString(i+1) + " ";
			for(int j =0;j<8;j++){
				if(pieces[i][j] == null){
					if(i%2 == 0){
						if(j%2 == 0){
							text += "■ ";
						}
						else{
							text += "□ ";
						}
					} else{
						if(j%2 == 0){
							text += "□ ";
						}
						else{
							text += "■ ";
						}
					}
					continue;
				}
				switch(pieces[i][j].getType()){
					case BISHOP:
						if(pieces[i][j].getColor() == Color.BLACK){
							text += "♝ ";
						}
						else{
							text += "♗ ";
						}
						break;
					case KING:
						if(pieces[i][j].getColor() == Color.BLACK){
							text += "♚ ";
						}
						else{
							text += "♔ ";
						}
						break;
					case KNIGHT:
						if(pieces[i][j].getColor() == Color.BLACK){
							text += "♞ ";
						}
						else{
							text += "♘ ";
						}
						break;
					case PAWN:
						if(pieces[i][j].getColor() == Color.BLACK){
							text += "♟ ";
						}
						else{
							text += "♙ ";
						}
						break;
					case QUEEN:
						if(pieces[i][j].getColor() == Color.BLACK){
							text += "♛ ";
						}
						else{
							text += "♕ ";
						}
						break;
					case ROOK:
						if(pieces[i][j].getColor() == Color.BLACK){
							text += "♜ ";
						}
						else{
							text += "♖ ";
						}
						break;
				}
			}
			System.out.println(text);
			response.add(text);
		}
		System.out.println();
		response.add("");
		return response;
	}
	
	/*
	 * This method will make the piece at Location oldLoc move to Location newLoc, if the move is valid.
	 * It will then flip the Color of currentTurn, signifying that the other player should now make a move.
	 */
	public List<String> makeMove(Location oldLoc, Location newLoc){
		List<String> response = new ArrayList<String>();
		if(!game){
			System.out.println("Game is over, start a new game to continue. \n");
			response.add("Game is over, start a new game to continue. \n");
			return response;
		}
		if(oldLoc.getHorizontal()> 7 || oldLoc.getHorizontal()<0 ||oldLoc.getVertical()> 7 || oldLoc.getVertical()<0 ||newLoc.getVertical()> 7 || newLoc.getVertical()<0 ||newLoc.getHorizontal()> 7 || newLoc.getHorizontal()<0){
			System.out.println("Location given is invalid. \n");
			response.add("Location given is invalid. \n");
			return response;
		}
		ChessPiece piece = pieces[oldLoc.getVertical()][oldLoc.getHorizontal()];
		if(piece == null){
			System.out.println("No piece in this square. \n");
			response.add("No piece in this square. \n");
			return response;
		}
		if(piece.nextStates(black,white).contains(newLoc)){
			if(blocking(oldLoc, newLoc, piece.getType())){

				System.out.println("Piece is blocked from making this move. \n");
				response.add("Piece is blocked from making this move. \n");
				return response;
			}
			if(checkIfRemove(oldLoc,newLoc,currentTurn) == false){
				ChessPiece taken = pieces[newLoc.getVertical()][newLoc.getHorizontal()];
				if(taken == null){
					pieces[oldLoc.getVertical()][oldLoc.getHorizontal()] = null;
					pieces[newLoc.getVertical()][newLoc.getHorizontal()] = piece;
					pieces[newLoc.getVertical()][newLoc.getHorizontal()].setLocation(newLoc);
					if(piece.getColor() == Color.WHITE){
						white.remove(oldLoc);
						white.put(newLoc, piece);
					} else{
						black.remove(oldLoc);
						black.put(newLoc, piece);
					}
					System.out.println("Successful move. \n");
					response.add("Successful move. \n");
				}
				else if(taken.getColor() == piece.getColor()){
					System.out.println("Trying to move to a square occupied by your own piece. \n");
					response.add("Trying to move to a square occupied by your own piece. \n");
					return response;
				}
				else{
					if(taken != null){
						if(taken.getColor() == Color.BLACK){
							black.remove(newLoc);
						}
						else{
							white.remove(newLoc);
						}
					}
					pieces[oldLoc.getVertical()][oldLoc.getHorizontal()] = null;
					pieces[newLoc.getVertical()][newLoc.getHorizontal()] = piece;
					pieces[newLoc.getVertical()][newLoc.getHorizontal()].setLocation(newLoc);
					if(piece.getColor() == Color.WHITE){
						white.remove(oldLoc);
						white.put(newLoc, piece);
					} else{
						black.remove(oldLoc);
						black.put(newLoc, piece);
					}
					System.out.println("Successful move. \n");
					response.add("Successful move. \n");
				}
			} else{
				System.out.println("If you move this piece you are in check.\n");
				response.add("If you move this piece you are in check.\n");
			}
		} else {
			System.out.println("This is not a valid move for this piece.\n");
			response.add("This is not a valid move for this piece.\n");
		}
		if(checkMate(currentTurn)){
			System.out.println("Checkmate.");
			response.add("Checkmate.");
			game = false;
		}
		if(currentTurn == Color.WHITE){
			currentTurn = Color.BLACK;
		}
		else{
			currentTurn = Color.WHITE;
		}
		response.addAll(printBoard());
		return response;
	}
	
	/*
	 * Given a Color c (the attacking team) and using the current board.
	 * Check if the other team is now in checkmate.
	 */
	private boolean checkMate(Color c) {
		Map<Location, ChessPiece> defending;
		ArrayList<Location> oldLocs = new ArrayList<Location>();
		ArrayList<Location> newLocs = new ArrayList<Location>();
		
		if(c == Color.WHITE){
			defending = black;
		}
		else{
			defending = white;
		}
		for(ChessPiece p : defending.values()){
			for(Location l : p.nextStates(black,white)){
				if(blocking(p.getLocation(), l, p.getType())){
					continue;
				}
				oldLocs.add(p.getLocation());
				newLocs.add(l);
			}
		}
		ArrayList<Location> toRemove = new ArrayList<Location>();
		for(int i = 0; i<oldLocs.size();i++){
			ChessPiece piece = pieces[oldLocs.get(i).getVertical()][oldLocs.get(i).getHorizontal()];
			ChessPiece newPiece = pieces[newLocs.get(i).getVertical()][newLocs.get(i).getHorizontal()];
			pieces[oldLocs.get(i).getVertical()][oldLocs.get(i).getHorizontal()] = null;
			pieces[newLocs.get(i).getVertical()][newLocs.get(i).getHorizontal()] = piece;
				
			if(c == Color.WHITE){
				if(checkIfRemove(oldLocs.get(i), newLocs.get(i), Color.BLACK) == true){
					toRemove.add(oldLocs.get(i));
				}
			}
			else{
				if(checkIfRemove(oldLocs.get(i), newLocs.get(i), Color.WHITE) == true){
					toRemove.add(oldLocs.get(i));
				}
			}
			
			pieces[oldLocs.get(i).getVertical()][oldLocs.get(i).getHorizontal()] = piece;
			pieces[newLocs.get(i).getVertical()][newLocs.get(i).getHorizontal()] = newPiece;
		}
		oldLocs.removeAll(toRemove);
		System.out.println(oldLocs.size());
		return oldLocs.size() == 0;
	}

	/*
	 * This method checks if a piece of Type t moving from location oldLoc to location newLoc
	 * will encounter any other pieces along the way, therefore making this move invalid.
	 */
	public boolean blocking(Location oldLoc, Location newLoc, Type t) {
		if(t==Type.KNIGHT){
			return false;
		}
		boolean increasingVertical = oldLoc.getVertical()<newLoc.getVertical();
		boolean increasingHorizontal = oldLoc.getHorizontal()<newLoc.getHorizontal();
		int horizontalDifference = oldLoc.getHorizontal()-newLoc.getHorizontal();
		int verticalDifference = oldLoc.getVertical()-newLoc.getVertical();
		if(oldLoc.getHorizontal() == newLoc.getHorizontal()){
			if(increasingVertical){
				for(int i=oldLoc.getVertical()+1;i<newLoc.getVertical();i++){
					if(pieces[i][oldLoc.getHorizontal()] != null){
						System.out.println("here1");
						return true;
					}
				}
			} else {
				for(int i=oldLoc.getVertical()-1;i>newLoc.getVertical();i--){
					if(pieces[i][oldLoc.getHorizontal()] != null){
						System.out.println("here2");
						return true;
					}
				}
			}
		}
		if(oldLoc.getVertical() == newLoc.getVertical()){
			if(increasingHorizontal){
				for(int i=oldLoc.getHorizontal()+1;i<newLoc.getHorizontal();i++){
					if(pieces[oldLoc.getVertical()][i] != null){
						System.out.println("here3");
						return true;
					}
				}
			} else {
				for(int i=oldLoc.getHorizontal()-1;i>newLoc.getHorizontal();i--){
					if(pieces[oldLoc.getVertical()][i] != null){
						System.out.println("here4");
						return true;
					}
				}
			}
		}
		if(horizontalDifference-verticalDifference == 0){
			if(increasingVertical){
				if(increasingHorizontal){
					for(int i=1;i<verticalDifference;i++){
						if(pieces[oldLoc.getVertical()+i][oldLoc.getHorizontal()+i] != null){
							return true;
						}
					}
				}
				else{
					for(int i=1;i<verticalDifference;i++){
						if(pieces[oldLoc.getVertical()+i][oldLoc.getHorizontal()-i] != null){
							return true;
						}
					}
				}
			}
			else{
				if(increasingHorizontal){
					for(int i=1;i<verticalDifference;i++){
						if(pieces[oldLoc.getVertical()-i][oldLoc.getHorizontal()+i] != null){
							return true;
						}
					}
				}
				else{
					for(int i=1;i<verticalDifference;i++){
						if(pieces[oldLoc.getVertical()-i][oldLoc.getHorizontal()-i] != null){
							return true;
						}
					}
				}
			}
		}
		return false;
	}

	/*
	 * With a team Color team and a current Piece location of l.
	 * This method will check if removing said piece will cause the user to be in check.
	 * Therefore an invalid move.
	 */
	public boolean checkIfRemove(Location l, Location newLoc, Color team){
		ChessPiece piece = pieces[l.getVertical()][l.getHorizontal()];
		pieces[l.getVertical()][l.getHorizontal()] = null;
		Map<Location,ChessPiece> attackers;
		if(team == Color.WHITE){
			attackers = black;
		}
		else{
			attackers = white;
		}
		//System.out.println(attackers.values());
		for(ChessPiece p : attackers.values()){
			if(p.getLocation().equals(newLoc)){
				continue;
			}
			if(attackers == black){
				if(p.nextStates(black,white).contains(whiteKing.getLocation())){
					if(!blocking(p.getLocation(), whiteKing.getLocation(),p.getType())){
						pieces[l.getVertical()][l.getHorizontal()] = piece;
						return true;
					}
				}
			}
			else{
				if(p.nextStates(black,white).contains(blackKing.getLocation())){
					if(!blocking(p.getLocation(), blackKing.getLocation(),p.getType())){
						pieces[l.getVertical()][l.getHorizontal()] = piece;
						return true;
					}
				}
			}
		}
		pieces[l.getVertical()][l.getHorizontal()] = piece;
		return false;
	}
	
	public ChessPiece[][] getBoard(){
		return pieces;
	}
	public HashMap<Location, ChessPiece> getBlack(){
		return black;
	}
	public HashMap<Location, ChessPiece> getWhite(){
		return white;
	}
	
	public Color getCurrentTurn(){
		return currentTurn;
	}
	
	public static void main(String args[]){
		Board board = new Board();
		board.makeMove(new Location(1,4), new Location(3,4));
		board.makeMove(new Location(6,2), new Location(5,2));
		board.makeMove(new Location(0,1), new Location(2,2));
		board.makeMove(new Location(6,3), new Location(4,3));
		board.makeMove(new Location(0,6), new Location(2,5));
		board.makeMove(new Location(4,3), new Location(3,4));
		board.makeMove(new Location(2,2), new Location(3,4));
		board.makeMove(new Location(7,6), new Location(5,5));
		board.makeMove(new Location(0,3), new Location(1,4));
		board.makeMove(new Location(7,1), new Location(6,3));
		//board.makeMove(new Location(,), new Location(,));
		//board.makeMove(new Location(,), new Location(,));
		//board.makeMove(new Location(,), new Location(,));

	}
}
