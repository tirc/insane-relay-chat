package game;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


public class Pawn extends ChessPiece{

	public Pawn(Type t, Location l, Color c) {
		super(t, l, c);
	}

	@Override
	public Set<Location> nextStates(HashMap<Location,ChessPiece> black, HashMap<Location,ChessPiece> white) {
		Set<Location> next = new HashSet<Location>();
		Location tempLocation;
		if(color == Color.BLACK){
			if(getPreviousMoves().isEmpty()){
				tempLocation = new Location(location.getVertical()-2,location.getHorizontal());
				if(!black.keySet().contains(tempLocation) && !white.keySet().contains(tempLocation)){
					next.add(tempLocation);
				}
			}
			tempLocation = new Location(location.getVertical()-1, location.getHorizontal());
			if(!black.keySet().contains(tempLocation) && !white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()-1, location.getHorizontal()+1);
			if(white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			Location enPassantCheck = new Location(location.getVertical(), location.getHorizontal()+1);
			if(white.keySet().contains(enPassantCheck)){
				if(white.get(enPassantCheck).getType() == Type.PAWN && white.get(enPassantCheck).getPreviousMoves().size() == 1 && white.get(enPassantCheck).getPreviousMoves().get(0).getVerticalLength() == 2){
					next.add(tempLocation);
				}
			}
			tempLocation = new Location(location.getVertical()-1, location.getHorizontal()-1);
			if(white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			enPassantCheck = new Location(location.getVertical(), location.getHorizontal()-1);
			if(white.keySet().contains(enPassantCheck)){
				if(white.get(enPassantCheck).getType() == Type.PAWN && white.get(enPassantCheck).getPreviousMoves().size() == 1 && white.get(enPassantCheck).getPreviousMoves().get(0).getVerticalLength() == 2){
					next.add(tempLocation);
				}
			}
		}
		else{
			if(getPreviousMoves().isEmpty()){
				tempLocation = new Location(location.getVertical()+2, location.getHorizontal());
				if(!black.keySet().contains(tempLocation) && !white.keySet().contains(tempLocation)){
					next.add(tempLocation);
				}
			}
			tempLocation = new Location(location.getVertical()+1, location.getHorizontal());
			if(!black.keySet().contains(tempLocation) && !white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()+1, location.getHorizontal()+1);
			if(black.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			Location enPassantCheck = new Location(location.getVertical(), location.getHorizontal()+1);
			if(black.keySet().contains(enPassantCheck)){
				if(black.get(enPassantCheck).getType() == Type.PAWN && black.get(enPassantCheck).getPreviousMoves().size() == 1 && black.get(enPassantCheck).getPreviousMoves().get(0).getVerticalLength() == 2){
					next.add(tempLocation);
				}
			}
			tempLocation = new Location(location.getVertical()+1, location.getHorizontal()-1);
			if(black.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			enPassantCheck = new Location(location.getVertical(), location.getHorizontal()-1);
			if(black.keySet().contains(enPassantCheck)){
				if(black.get(enPassantCheck).getType() == Type.PAWN && black.get(enPassantCheck).getPreviousMoves().size() == 1 && black.get(enPassantCheck).getPreviousMoves().get(0).getVerticalLength() == 2){
					next.add(tempLocation);
				}
			}
		}
		
		Set<Location> tempSet = new HashSet<Location>();
		for(Location l : next){
			if(l.getHorizontal() < 0 || l.getHorizontal() >7 || l.getVertical() <0 || l.getVertical() >7){
				tempSet.add(l);
			}
		}
		next.removeAll(tempSet);
		return next;
	}

}
