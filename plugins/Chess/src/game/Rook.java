package game;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


public class Rook extends ChessPiece{

	public Rook(Type t, Location l, Color c) {
		super(t, l, c);
	}

	@Override
	public Set<Location> nextStates(HashMap<Location,ChessPiece> black, HashMap<Location, ChessPiece> white) {
		Set<Location> next = new HashSet<Location>();
		Location tempLocation;
		if(color == Color.WHITE){
			for(int i = 0; i<8; i++){
				tempLocation = new Location(location.getVertical(), i);
				if(!white.keySet().contains(tempLocation)){
					next.add(tempLocation);
				}
			}
			for(int i = 0; i<8; i++){
				tempLocation = new Location(i, location.getHorizontal());
				if(!white.keySet().contains(tempLocation)){
					next.add(tempLocation);
				}
			}
		} else{
			for(int i = 0; i<8; i++){
				tempLocation = new Location(location.getVertical(), i);
				if(!black.keySet().contains(tempLocation)){
					next.add(tempLocation);
				}
			}
			for(int i = 0; i<8; i++){
				tempLocation = new Location(i, location.getHorizontal());
				if(!black.keySet().contains(tempLocation)){
					next.add(tempLocation);
				}
			}
		}

		Set<Location> tempSet = new HashSet<Location>();
		for(Location l : next){
			if(l.getHorizontal() < 0 || l.getHorizontal() >7 || l.getVertical() <0 || l.getVertical() >7){
				tempSet.add(l);
			}
		}
		next.removeAll(tempSet);
		return next;
	}

}
