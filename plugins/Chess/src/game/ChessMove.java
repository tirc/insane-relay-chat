package game;

public class ChessMove {

	private Location initial;
	private Location end;
	private int verticalLength;
	private int horizontalLength;
	
	public ChessMove(Location i, Location e){
		initial = i;
		end = e;
		verticalLength = Math.abs(i.getVertical() - e.getVertical());
		horizontalLength = Math.abs(i.getHorizontal() - e.getHorizontal());
	}
	public Location getStart(){
		return initial;
	}
	public Location getEnd(){
		return end;
	}
	public int getVerticalLength(){
		return verticalLength;
	}
	public int getHorizontalLenght(){
		return horizontalLength;
	}
}
