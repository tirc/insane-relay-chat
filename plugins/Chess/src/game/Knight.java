package game;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;


public class Knight extends ChessPiece{

	public Knight(Type t, Location l, Color c) {
		super(t, l, c);
	}

	@Override
	public Set<Location> nextStates(HashMap<Location,ChessPiece> black, HashMap<Location, ChessPiece> white){
		Set<Location> next = new HashSet<Location>();
		Location tempLocation;
		if(color == Color.WHITE){
			tempLocation = new Location(location.getVertical()+2, location.getHorizontal()+1);
			if(!white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()+2, location.getHorizontal()-1);
			if(!white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()+1, location.getHorizontal()+2);
			if(!white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()+1, location.getHorizontal()-2);
			if(!white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()-2, location.getHorizontal()+1);
			if(!white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()-2, location.getHorizontal()-1);
			if(!white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()-1, location.getHorizontal()+2);
			if(!white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()-1, location.getHorizontal()-2);
			if(!white.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
		}
		else{
			tempLocation = new Location(location.getVertical()+2, location.getHorizontal()+1);
			if(!black.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()+2, location.getHorizontal()-1);
			if(!black.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()+1, location.getHorizontal()+2);
			if(!black.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()+1, location.getHorizontal()-2);
			if(!black.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()-2, location.getHorizontal()+1);
			if(!black.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()-2, location.getHorizontal()-1);
			if(!black.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()-1, location.getHorizontal()+2);
			if(!black.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			tempLocation = new Location(location.getVertical()-1, location.getHorizontal()-2);
			if(!black.keySet().contains(tempLocation)){
				next.add(tempLocation);
			}
			
		}
		
		Set<Location> tempSet = new HashSet<Location>();
		for(Location l : next){
			if(l.getHorizontal() < 0 || l.getHorizontal() >7 || l.getVertical() <0 || l.getVertical() >7){
				tempSet.add(l);
			}
		}
		next.removeAll(tempSet);
		return next;
	}
}
