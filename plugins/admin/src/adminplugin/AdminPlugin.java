package adminplugin;

import ircserver.Client;
import ircserver.InternalCommandMessage.Command;
import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;
import ircserver.plugin.AbstractPlugin;
import ircserver.plugin.InternalMessageFactory;
import ircserver.plugin.InternalMessageFactoryFactory;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.esotericsoftware.minlog.Log;

public class AdminPlugin extends AbstractPlugin {

	private static final String NAME = "admin-authenticator";
	private static final String VERSION = "1.0";
	
	private static final String COMMAND_ADMIN_AUTH = "admin-auth";
	private static final List<String> COMMANDS = Arrays.asList(COMMAND_ADMIN_AUTH);
	private static final List<String> HELP = Arrays.asList(COMMAND_ADMIN_AUTH + " <admin-key> :: Authenticate as admin with the admin key produced when the server started");
	
	private boolean allowAdminAuth;
	private final String generatedKey;
	private InternalMessageFactoryFactory factory;
	
	public AdminPlugin() {
		super(NAME, VERSION, COMMANDS, HELP);
		factory = new InternalMessageFactoryFactory(NAME);
		
		allowAdminAuth = true;
		generatedKey = generateKey();
		
		if (allowAdminAuth) {
			Log.info("Admin authentication key: " + generatedKey);
		}
	}
	
	private String generateKey() {
		return Long.toHexString(System.currentTimeMillis());
	}
	
	@Override
	public List<InternalMessage> response(InternalIRCMessage m) {
		List<InternalMessage> responses = new ArrayList<>();
		Client client = m.getClient();
		
		InternalMessageFactory factory = this.factory.build(client);
		
		String[] params = m.getMessage().params;
		
		if (params.length == 3) {
			if (allowAdminAuth) {
				String givenKey = params[2];
				
				if (givenKey.equals(generatedKey)) {
					responses.add(factory.newPrivateMessage("Correct admin key given, you are now identified as server administrator"));
					responses.add(factory.newCommandMessage(Command.SET_USER_MODE, "+A"));
				} else {
					responses.add(factory.newPrivateMessage("Incorrect admin key given"));
				}
			} else {
				responses.add(factory.newPrivateMessage("Admin authentication is not enabled"));
			}
		} else {
			responses.add(factory.newPrivateMessage("Invalid command, see 'help " + NAME + "'"));
		}

		return responses;
	}
	

}
