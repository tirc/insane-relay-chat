package chanserv;

import ircserver.IRCEvent;
import ircserver.InternalCommandMessage.Command;
import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;
import ircserver.Message;
import ircserver.plugin.AbstractPlugin;
import ircserver.plugin.InternalMessageFactory;
import ircserver.plugin.InternalMessageFactoryFactory;
import ircserver.plugin.PersistentStorage;
import ircserver.plugin.PluginDescriptor;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.esotericsoftware.minlog.Log;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@SuppressWarnings("unchecked")
public class ChanServPlugin extends AbstractPlugin{

	private static final String NAME = "chanserv";
	private static final String VERSION = "1.0";
	
	private static final String COMMAND_REGISTER = "cs-register";
	private static final String COMMAND_DROP = "cs-drop";
	private static final String COMMAND_SET = "cs-set";
	private static final String COMMAND_INFO = "cs-info";
	
	private static final String KEY_FOUNDER = "FOUNDER";
	private static final String KEY_OP = "OP";
	private static final String DB_DOCNAME = "channels";	
	private static final String DB_KEY_CHANLIST = "channels-list";
	
	private static final List<String> COMMANDS = Arrays.asList(
			COMMAND_REGISTER,
			COMMAND_DROP,
			COMMAND_SET,
			COMMAND_INFO
			);

	private static final List<String> HELP = Arrays.asList(
			COMMAND_REGISTER + "<channel> :: Claim a channel.",
			COMMAND_DROP + "<channel> :: Release a channel.",
			COMMAND_INFO + "<channel> :: Get info about a registered channel",
			COMMAND_SET + "<channel> " + KEY_FOUNDER + " <nickname> :: Change a channel's founder",
			COMMAND_SET + "<channel> " + KEY_OP + " <+|-><nickname> :: Add/remove operator in a channel"
			);
	
	private InternalMessageFactoryFactory factory;
	private Set<String> knownChannels;	// All channels on the server
	private List<Channel> managedChannels; // ChanServ managed channel entries
	private Gson gson;
	private int authPluginPort;
	private boolean enabled;
	
	public ChanServPlugin(){
		super(NAME, VERSION, COMMANDS, HELP);
		factory = new InternalMessageFactoryFactory(NAME);
		knownChannels = new HashSet<>();
		gson = new GsonBuilder().create();
		managedChannels = new ArrayList<>();
	}
	
	@Override
	public void init(PersistentStorage storage, PluginDescriptor... ps) {
		super.init(storage, ps);
		enabled = false;
		
		for (PluginDescriptor p : ps) {
			if (p.pluginName.equals("nickauth")) {
				enabled = true;
				authPluginPort = p.portNumber;
			}
		}
	
		String channelsListJson = storage.getValue(NAME, DB_DOCNAME, DB_KEY_CHANLIST);
		
		if (channelsListJson == null || channelsListJson.equals("")) {
			storage.setValue(NAME, DB_DOCNAME, DB_KEY_CHANLIST, gson.toJson(new ArrayList<String>()));
		} else {
			List<String> channels = gson.fromJson(channelsListJson, List.class);
			
			for (String chanName : channels) {
				String chanJson = storage.getValue(NAME, DB_DOCNAME, chanName);
				
				if (chanJson.equals(""))
					continue;
				
				Channel channel = gson.fromJson(chanJson, Channel.class);
				managedChannels.add(channel);
			}
		}
	}
	
	@Override
	public List<InternalMessage> response(InternalIRCMessage m) {
		List<InternalMessage> responses = new ArrayList<>();
		Message msg = m.getMessage();
		InternalMessageFactory factory = this.factory.build(m.getClient());
		
		if (!enabled) {
			responses.add(factory.newPrivateMessage("ChanServ is disabled"));
			return responses;
		}
		
		try {
			if (!userIsAuthenticated(m.getClient().getNick())) {
				responses.add(factory.newPrivateMessage("You need to be authenticated with nickauth to use chanserv"));
				return responses;
			}
		} catch (IOException e) {
			responses.add(factory.newPrivateMessage("Could not check authentication with nickauth, try again later"));
			return responses;
		}
		
		if (msg.params.length < 3) {
			responses.add(getHelpMessage(factory));
		} else {
			String command = msg.params[1].substring(2);
			System.out.println(command);
			
			switch (command) {
			case COMMAND_REGISTER:
				responses.addAll(handleRegister(msg, m, factory));
				break;
			case COMMAND_DROP:
				responses.addAll(handleDrop(msg, m, factory));				
				break;
			case COMMAND_SET:
				responses.addAll(handleSet(msg, m, factory));
				break;
			case COMMAND_INFO:
				responses.addAll(handleInfo(msg, m, factory));
				break;
			default:
				responses.add(getHelpMessage(factory));
			}
		}
		
		return responses;
	}

	private List<InternalMessage> handleRegister(Message msg, InternalMessage m, InternalMessageFactory factory) {
		List<InternalMessage> responses = new ArrayList<>();
		
		String chanName = msg.params[2];
		
		if (knownChannels.contains(chanName)) {
			responses.add(factory.newPrivateMessage("Cannot register a channel that already exists"));
		} else {
			Channel chan = new Channel(chanName, m.getClient().getNick());
			managedChannels.add(chan);
			
			storage.setValue(NAME, DB_DOCNAME, chanName, gson.toJson(chan));

			List<String> allChannels = gson.fromJson(storage.getValue(NAME, DB_DOCNAME, DB_KEY_CHANLIST), List.class);
			allChannels.add(chanName);
			storage.setValue(NAME, DB_DOCNAME, DB_KEY_CHANLIST, gson.toJson(allChannels));
			
			responses.add(factory.newPrivateMessage("Registered new channel " + chanName));
			responses.add(factory.newCommandMessage(Command.CREATE_CHANNEL, chanName));
			responses.add(factory.newCommandMessage(Command.USER_JOIN_CHANNEL, chanName, m.getClient().getNick()));
		}
		
		return responses;
	}
	
	private List<InternalMessage> handleDrop(Message msg, InternalMessage m, InternalMessageFactory factory) {
		List<InternalMessage> responses = new ArrayList<>();

		String chanName = msg.params[2];
		Channel chan = getManagedChannel(chanName);
		
		if (chan != null) {
			if (chan.getFounder().equals(m.getClient().getNick())) {
				storage.setValue(NAME, DB_DOCNAME, chanName, "");
				
				List<String> allChannels = gson.fromJson(storage.getValue(NAME, DB_DOCNAME, DB_KEY_CHANLIST), List.class);
				allChannels.remove(chanName);
				storage.setValue(NAME, DB_DOCNAME, DB_KEY_CHANLIST, gson.toJson(allChannels));
				
				responses.add(factory.newPrivateMessage("Channel dropped (it will continue to exist until the server is re-started)"));
			} else {
				responses.add(factory.newPrivateMessage("Channels can only be dropped by their founder"));
			}
		} else {
			responses.add(factory.newPrivateMessage(chanName + " not registered with ChanServ"));
		}
		
		return responses;
	}
	
	private List<InternalMessage> handleSet(Message msg, InternalMessage m, InternalMessageFactory factory) {
		List<InternalMessage> responses = new ArrayList<>();
		String chanName = msg.params[2];
		Channel chan = getManagedChannel(chanName);
		
		if (chan == null) {
			responses.add(factory.newPrivateMessage(chanName + " is not managed by ChanServ"));
			return responses;
		}
		
		if (!chan.getFounder().equals(m.getClient().getNick())) {
			responses.add(factory.newPrivateMessage("Only channel founders can use the set command"));
		}
		
		if (msg.params.length < 5) {
			responses.add(factory.newPrivateMessage("Malformed set command, see !help for details"));
		}
		
		if (responses.size() > 0) {
			return responses;
		}
		
		String key = msg.params[3];
		String value = msg.params[4];
		
		switch (key) {
		case KEY_FOUNDER:
			try {
				if (userIsAuthenticated(value)) {
					chan.setFounder(value);
					responses.add(factory.newPrivateMessage("Founder of " + chanName + " set to " + value));
				} else {
					responses.add(factory.newPrivateMessage("New founder is not authenticated with nickauth"));
				}
			} catch (IOException e) {
				responses.add(factory.newPrivateMessage("Could not contact nickauth to authenticate new founder"));
			}
			break;
		case KEY_OP:
			String nick = value.substring(1);
			if (value.startsWith("+")) {
				try {
					if (userIsAuthenticated(nick)) {
						if (chan.addOp(nick)) {
							responses.add(factory.newPrivateMessage(nick + " is now an op in " + chanName));
						} else {
							responses.add(factory.newPrivateMessage(nick + " is already an op in " + chanName));
						}
					} else {
						responses.add(factory.newPrivateMessage("New op must be authenticated with nickauth"));
					}
				} catch (IOException e) {
					responses.add(factory.newPrivateMessage(""));
				}
			} else if (value.startsWith("-")) {
				if (chan.removeOp(nick)) {
					responses.add(factory.newPrivateMessage(nick + " is no longer an op in " + chanName));
				} else {
					responses.add(factory.newPrivateMessage(nick + " is not an op in " + chanName));
				}
			} else {
				responses.add(factory.newPrivateMessage("Malformed operator set command, use +username or -username to add/remove"));
			}
			break;
		default:
			responses.add(factory.newPrivateMessage("Unknown set command key: " + key));
		}
		
		storage.setValue(NAME, DB_DOCNAME, chanName, gson.toJson(chan));
		
		return responses;
	}
	
	private List<InternalMessage> handleInfo(Message msg, InternalMessage m, InternalMessageFactory factory) {
		List<InternalMessage> responses = new ArrayList<>();
		
		String chanName = msg.params[2];
		Channel chan = getManagedChannel(chanName);
		
		if (chan != null) {
			responses.add(factory.newPrivateMessage(chanName + ": Registered with ChanServ"));
			responses.add(factory.newPrivateMessage("Created: " + chan.getDate()));
			responses.add(factory.newPrivateMessage("Founder: " + chan.getFounder()));
			
			for (String opNick : chan.getOps()) {
				responses.add(factory.newPrivateMessage("Operator: " + opNick));
			}
		} else {
			responses.add(factory.newPrivateMessage(chanName + ": Not registered with ChanServ"));
		}
		
		return responses;	
	}
	
	private Channel getManagedChannel(String chanName) {
		for (Channel c : managedChannels) {
			if (c.getName().equals(chanName)) {
				return c;
			}
		}
		
		return null;
	}
	
	@Override
	public List<InternalMessage> handleEvent(IRCEvent event) {
		List<InternalMessage> responses = new ArrayList<>();
		InternalMessageFactory factory = this.factory.build(event.getClient());
		
		switch (event.getType()) {
		case SERVER_INITIALISED:
			for (Channel c : managedChannels) {
				responses.add(factory.newCommandMessage(Command.CREATE_CHANNEL, c.getName()));
				Log.debug("ChanServ: Creating managed channel " + c.getName());
			}
			break;
		case CHANNEL_CREATED:
			String chanName = event.getParams()[0];
			knownChannels.add(chanName);
			break;
		case USER_JOIN_CHANNEL:
			String chan = event.getParams()[0];
			String nick = event.getParams()[1];
			responses.addAll(handleChannelJoin(chan, nick, factory));
			break;
		default:
			break;
		}
		
		return responses;
	}
	
	private List<InternalMessage> handleChannelJoin(String chanName, String nick, InternalMessageFactory factory) {
		List<InternalMessage> responses = new ArrayList<>();
		
		Channel chan = getManagedChannel(chanName);
		
		if (chan != null) {
			if (chan.getOps().contains(nick) || chan.getFounder().equals(nick)) {
				try {
					if (userIsAuthenticated(nick)) {
						responses.add(factory.newCommandMessage(Command.SET_CHANNEL_USER_MODE, chanName, "+o"));
						responses.add(factory.newPrivateMessage("You have received operator status in " + chanName));
						Log.info("ChanServ: Giving operator status to " + nick + " in " + chanName);
					} 
				}  catch (IOException e) {
					Log.debug("ChanServ: couldn't communicate with nickauth to check operator authentication");
				}
			}
		}
		
		return responses;
	}

	private boolean userIsAuthenticated(String nick) throws UnknownHostException, IOException {
		Socket sock = new Socket("127.0.0.1", authPluginPort);
		sock.getOutputStream().write(("ISAUTHED " + nick + "\n").getBytes());
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(sock.getInputStream()));
		String response = reader.readLine();
		sock.close();
		
		return response.startsWith("AUTHED");
	}
	
	private InternalMessage getHelpMessage(InternalMessageFactory factory) {
		return factory.newPrivateMessage("See !help " + NAME + " for help");
	}
}
