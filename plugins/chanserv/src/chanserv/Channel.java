package chanserv;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Channel {

	private String name;
	private String founder;
	private Date registerDate;
	private List<String> operators;
	
	public Channel(String name, String founder) {
		this.name = name;
		this.founder = founder;
		registerDate = new Date();
		operators = new ArrayList<>();
	}

	public String getName() {
		return name;
	}
	
	public String getFounder() {
		return founder;
	}

	public void setFounder(String founder) {
		this.founder = founder;		
	}

	public boolean addOp(String nick) {
		if (operators.contains(nick)) {
			return false;
		} else {
			operators.add(nick);
			return true;
		}
	}

	public boolean removeOp(String nick) {
		if (operators.contains(nick)) {
			operators.remove(nick);
			return true;
		} else {
			return false;
		}
	}

	public List<String> getOps() {
		return operators;
	}

	public String getDate() {
		return registerDate.toString();
	}
}
