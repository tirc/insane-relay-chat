package servercurrency;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;

import ircserver.plugin.PersistentStorage;

public class ServerCurrencyService {

	public static final String KEY_MONEY = "money";
	private PersistentStorage storage;
	private Socket auth;
	private boolean authMode;

	public ServerCurrencyService(PersistentStorage storage, int authPort) {
		this.storage = storage;
		if (authPort == -1) {
			authMode = false;
		} else {
			try {
				authMode = true;
				auth = new Socket(InetAddress.getLocalHost(), authPort);
			} catch (IOException e) {
				if (auth != null) {
					try {
						auth.close();
					} catch (IOException ignored) {
					}
				}
			}
		}
	}

	public int getCash(String nick) {
		String cashString = storage.getValue(ServerCurrencyPlugin.NAME, nick,
				KEY_MONEY);
		int cash;
		try {
			cash = cashString.equals("") ? 0 : Integer.parseInt(cashString);
		} catch (NumberFormatException e) {
			cash = 0;
		}

		return cash;
	}
	
	private boolean confirmAuthed(String nick){
		if(!authMode)
			return true;

		BufferedReader reader = null;
		
		if(!auth.isClosed()) {
			try {
				auth.getOutputStream().write(String.format("ISAUTHED %s\n", nick).getBytes());
			
				reader = new BufferedReader(new InputStreamReader(auth.getInputStream()));
				String line = reader.readLine();
				
				return line.equals(String.format("AUTHED %s", nick));
			} catch (IOException ignored) {
				
			}
			
		}
		
		return false;
	}
	
	public synchronized boolean compareAndSwap(String nick, int compare, int swap) {
		if(!confirmAuthed(nick))
			return false;
		
		int cash = getCash(nick);
		
		if (compare == cash && swap >= 0) {
			storage.setValue(ServerCurrencyPlugin.NAME, nick, KEY_MONEY, Integer.toString(swap));
			return true;
		}
		
		return false;
	}
	
	public boolean isAuthMode() {
		return authMode;
	}
}
