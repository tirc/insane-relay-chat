package servercurrency;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ServerCurrencyRequestHandler implements Runnable {
	private ServerCurrencyService service;
	private Socket client;
	
	private static final int MAX_TRIES = 3;
	
	public ServerCurrencyRequestHandler(Socket client, ServerCurrencyService service){
		this.client = client;
		this.service = service;
	}

	private void handleCommand(String line) {
		String[] splits = line.split("\\s+");
		try{
			String command = splits[0];
			switch(command){
			case "AUTHMODE":
				respond(String.format("AUTHMODE %s", service.isAuthMode()?"ENABLED":"DISABLED"));
				break;
			case "LOOKUP":
				respond(String.format("LOOKUP %s %s", splits[1], service.getCash(splits[1])));
				break;
			case "WITHDRAWMONEY":
				int withdraw = Integer.parseInt(splits[2]);
				if(withdraw(splits[1], withdraw))
					respond(String.format("WITHDRAWSUCCEED %s %s", splits[1], withdraw));
				else
					respond("ERROR WITHDRAWFAILED");
				break;
			case "DEPOSITMONEY":
				int deposit = Integer.parseInt(splits[2]);
				if(deposit(splits[1], deposit))
					respond(String.format("DEPOSITSUCCEED %s %s", splits[1], deposit));
				else
					respond("ERROR DEPOSITFAILED");
				break;
			}
		} catch (ArrayIndexOutOfBoundsException e){
			respond("ERROR PARAMETERSMISSING");
		}
	}
	
	private boolean deposit(String nick, int quantity){
		for(int i = 0; i < MAX_TRIES; i++){
			int compare = service.getCash(nick);
			int swap = compare + quantity;

			if(swap < 0)
				return false;
			if(service.compareAndSwap(nick, compare, swap))
				return true;
		}
		return false;
	}
	
	private boolean withdraw(String nick, int quantity){
		return deposit(nick, -quantity);
	}

	private void respond(String message) {
		try {
			client.getOutputStream().write(message.getBytes());
		} catch (IOException e) {}
	}
	
	
	@Override
	public void run() {
		if(!client.isClosed())
			try{
				BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream()));
				String line;
				while((line = br.readLine()) != null)
					handleCommand(line);
			} catch (IOException ignored){}
	}

}
