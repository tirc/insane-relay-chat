package servercurrency;

import ircserver.Client;
import ircserver.IRCEvent;
import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;
import ircserver.Message;
import ircserver.plugin.InternalMessageFactory;
import ircserver.plugin.InternalMessageFactoryFactory;
import ircserver.plugin.PersistentStorage;
import ircserver.plugin.Plugin;
import ircserver.plugin.PluginDescriptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class ServerCurrencyPlugin implements Plugin{

	public static final String NAME = "servercurrency";
	private static final String VERSION = "0.0.1a";
	private static final List<String> COMMANDS = Arrays
			.asList("money-pay",
					"money-lookup");
	private static final List<String> HELP = Arrays
			.asList("money-pay <nick> <quantity>:: Pays nick quantity",
					"money-lookup <nick> :: Find out how much money nick has");
	
	private static final int MAX_TRIES = 3;
	private InternalMessageFactoryFactory factory;
	private ServerCurrencyServer currencyServer;
	private ServerCurrencyService currencyService;
	
	@Override
	public void init(PersistentStorage p, PluginDescriptor... ps) {
		int authPort = -1;
		
		for(PluginDescriptor plugin : ps){
			if(plugin.pluginName.equals("nickauth")){
				authPort = plugin.portNumber;
			}
		}
		
		currencyService = new ServerCurrencyService(p, authPort);
		currencyServer = new ServerCurrencyServer(currencyService);
		
		factory = new InternalMessageFactoryFactory(NAME);
		
		new Thread(currencyServer).start();
	}
	
	@Override
	public List<InternalMessage> response(InternalIRCMessage m) {
		List<InternalMessage> responses = new ArrayList<>();
		Message msg = m.getMessage();
		
		Client client = m.getClient();
		InternalMessageFactory factory = this.factory.build(client);
		factory.setRecipient(client.getNick());
		
		String command = msg.params[1].substring(2);
		switch(command){
		case "money-pay":
			if(msg.params.length < 4)
				responses.add(factory.newPrivateMessage("See !help " + NAME));
			else
				moneyPay(responses, client.getNick(), msg.params[2], Integer.parseInt(msg.params[3]), factory);
			break;
		case "money-lookup":
			if(msg.params.length < 3)
				responses.add(factory.newPrivateMessage("See !help " + NAME));
			else
				responses.add(factory.newPrivateMessage(String.format("%s has %s", msg.params[2] ,currencyService.getCash(msg.params[2]))));
			break;
		}
		
		
		return responses;
	}


	private void moneyPay(List<InternalMessage> responses, String nickSource, String nickTarget, int quantity, InternalMessageFactory factory){
		
		if(quantity == 0){
			responses.add(factory.newPrivateMessage("You can't pay someone nothing!"));
			return;
		}
		if(quantity < 0){
			responses.add(factory.newPrivateMessage("That would be stealing!"));
			return;
		}
		int cashSource = currencyService.getCash(nickSource);
		int cashTarget = currencyService.getCash(nickTarget);
		if(currencyService.compareAndSwap(nickSource, cashSource, cashSource-quantity)){
			int i = 0;
			while(i < MAX_TRIES){
				if(currencyService.compareAndSwap(nickTarget, cashTarget, cashTarget+quantity)){
						responses.add(factory.newPrivateMessage(String.format("Transaction successful - you sent %s to %s.", quantity, nickTarget)));
						factory.setRecipient(nickTarget);
						responses.add(factory.newPrivateMessage(String.format("You just recieved %s from %s", quantity, nickSource)));
						factory.setRecipient(nickSource);
						return;
				}
				cashTarget = currencyService.getCash(nickTarget);
				i++;
			}
			i = 0;
			while(i < MAX_TRIES){
				cashSource = currencyService.getCash(nickSource);
				if(currencyService.compareAndSwap(nickSource, cashSource, cashSource+quantity)){
					responses.add(factory.newPrivateMessage("Transaction failed but your cash has been restored."));
					return;
				}
				i++;
			}
			responses.add(factory.newPrivateMessage("Well this is embarrassing. We couldn't deliver you cash or return it to you! Please seek help from a server administrator."));
			return;
		}
		responses.add(factory.newPrivateMessage("Sorry, we were unable to take the money from your account. Please try again."));
		return;
	}
	
	@Override
	public PluginDescriptor getPluginDescriptor() {
		return new PluginDescriptor(currencyServer.getPortNum(), NAME, VERSION, COMMANDS, HELP);
	}

	@Override
	public List<InternalMessage> handleEvent(IRCEvent event) {
		return new ArrayList<>();
	}

}