package servercurrency;

import java.io.IOException;
import java.net.ServerSocket;

public class ServerCurrencyServer implements Runnable {

	private ServerCurrencyService service;
	private ServerSocket ss;
	
	public ServerCurrencyServer(ServerCurrencyService service) {
		this.service = service;
		try {
			ss = new ServerSocket(0);
		} catch (IOException ignored) {}
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				ServerCurrencyRequestHandler handler = new ServerCurrencyRequestHandler(ss.accept(), service);
				new Thread(handler).start();
			} catch (IOException e) {}
		}
	}

	public int getPortNum() {
		return ss.getLocalPort();
	}
	
	

}
