package dateplugin;

import ircserver.Client;
import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;
import ircserver.plugin.AbstractPlugin;
import ircserver.plugin.InternalMessageFactory;
import ircserver.plugin.InternalMessageFactoryFactory;
import ircserver.plugin.PersistentStorage;
import ircserver.plugin.PluginDescriptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class DatePlugin extends AbstractPlugin {

	private static final String NAME = "date";
	private static final String VERSION = "1.0";
	private static final List<String> COMMANDS = Arrays.asList("date");
	private static final List<String> HELP = Arrays.asList("date    Shows current time and date");
	private InternalMessageFactoryFactory factory;

	public DatePlugin() {
		super(NAME, VERSION, COMMANDS, HELP);
	}
	
	@Override
	public void init(PersistentStorage p, PluginDescriptor... ps) {
		factory = new InternalMessageFactoryFactory(getPluginDescriptor().pluginName);
	}
	
	@Override
	public List<InternalMessage> response(InternalIRCMessage m) {
		List<InternalMessage> responses = new ArrayList<>();
		
		Client client = m.getClient();
		
		InternalMessageFactory factory = this.factory.build(client);
		
		responses.add(factory.newPrivateMessage(new Date().toString()));
		return responses;		
	} 
	
	@Override
	public PluginDescriptor getPluginDescriptor() {
		return new PluginDescriptor(-1, NAME, VERSION, COMMANDS, HELP);
	}
	
}
