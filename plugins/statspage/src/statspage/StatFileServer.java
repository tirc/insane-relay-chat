package statspage;

import webserver.TinyFileServer;

import com.google.gson.Gson;

public class StatFileServer implements TinyFileServer {

	protected static final String PAGE = "/stat.html";
	private static final String CSS = "/style.css";
	private static final String DATA = "/data.json";

	private final byte[] PAGEBYTES = "<!DOCTYPE html><html lang='en'><head><title>Insane Relay Chat Stats Plugin</title><link href='style.css' rel='stylesheet'><link rel='stylesheet' href='//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css'></head><body><div class='circle' id='users'> ? users </div></div></body><script src='http://code.jquery.com/jquery-2.0.2.min.js'></script><script src='//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js'></script><script>loadData();setInterval(loadData, 1000);function loadData(){$.getJSON('data.json', function ( data ){$('#users').text(data.users + ' users');});}</script></html>"
			.getBytes();
	private final byte[] CSSBYTES = ".circle{background-color:#0d8fdb;border-radius:50%;behavior: url(PIE.htc);width : 200px;height : 200px;text-align: center;vertical-align: middle;line-height: 200px;font-size:350%;color: #fbf2f3;}"
			.getBytes();

	private Data data;
	private Gson gson;

	private class Data {
		@SuppressWarnings("unused")
		public int users;
	}

	public StatFileServer() {
		data = new Data();

		gson = new Gson();
	}

	public void incUsers() {
		data.users++;
	}

	public void decUsers() {
		data.users--;
	}

	@Override
	public byte[] forFilePath(String path) {
		switch (path) {
		case PAGE:
			return PAGEBYTES;
		case CSS:
			return CSSBYTES;
		case DATA:
			return gson.toJson(data, Data.class).getBytes();
		default:
			return ERROR_404;
		}
	}

}
