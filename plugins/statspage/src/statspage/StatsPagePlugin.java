package statspage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import webserver.TinyHttpServer;

import ircserver.IRCEvent;
import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;
import ircserver.plugin.AbstractPlugin;

public class StatsPagePlugin extends AbstractPlugin {

	private static final String NAME = "StatsPagePlugin";
	private static final String VERSION = "1.0";

	private static final List<String> COMMANDS = Arrays.asList();
	private static final List<String> HELP = Arrays.asList();

	private StatFileServer sfs;
	private TinyHttpServer ths;

	public StatsPagePlugin() {
		super(NAME, VERSION, COMMANDS, HELP);
		sfs = new StatFileServer();
		try {
			ths = new TinyHttpServer(sfs, 0);
		} catch (IOException e) {
			ths = null;
		}
		if (ths != null) {
			System.out.println("http server started see: http://localhost:" + ths.getPort() + StatFileServer.PAGE);
			ths.start();
		} else {
			System.out.println("http server failed to start.");
		}
	}

	@Override
	public List<InternalMessage> response(InternalIRCMessage m) {
		return new ArrayList<>();
	}

	@Override
	public List<InternalMessage> handleEvent(IRCEvent event) {
		switch (event.getType()) {
		case USER_JOIN:
			sfs.incUsers();
			break;
		case USER_QUIT:
			sfs.decUsers();
			break;
		default:
			break;
		}
		return new ArrayList<>();
	}

}
