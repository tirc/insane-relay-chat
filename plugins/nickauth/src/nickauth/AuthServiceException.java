package nickauth;

public class AuthServiceException extends Exception{

	private static final long serialVersionUID = 3259342523870296185L;

	public AuthServiceException(String msg) {
		super(msg);
	}

}
