package nickauth;

import ircserver.plugin.PersistentStorage;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashSet;
import java.util.Set;

public class AuthService {
	
	private static final String ERROR_NO_SUCH_USER = "No such user registered.";
	private static final String ERROR_ALREADY_AUTHED = "User already authenticated.";
	private static final String ERROR_ALREADY_REGISTERED = "User already registered.";
	private static final String ERROR_INVALID_PASSWORD = "Invalid password.";
	private static final String ERROR_INVALID_REAL_NAME = "Invalid real name.";
	
	private static final String KEY_PASS = "password";
	private static final String KEY_NAME = "name";
	private PersistentStorage storage;
	private Set<String> authenticatedNicks;
	
	public AuthService(PersistentStorage storage) {
		this.storage = storage;
		authenticatedNicks = new HashSet<>();
	}
	
	public synchronized boolean authenticate(String nick, String rawPass) throws AuthServiceException {
		String storedHashPass = storage.getValue(NickAuthPlugin.NAME, nick, KEY_PASS);
		
		if(storedHashPass.equals("")) {
			throw new AuthServiceException(ERROR_NO_SUCH_USER);
		}
		
		if(authenticatedNicks.contains(nick)) {
			throw new AuthServiceException(ERROR_ALREADY_AUTHED);
		}
		
		String hashPass = hashPassword(rawPass);
		
		if(hashPass.equals(storedHashPass)){
			authenticatedNicks.add(nick);
			return true;
		}
		
		return false;
	}
	
	public synchronized boolean register(String nick, String rawPass, String fullName) throws AuthServiceException {
		if(isRegistered(nick)) {
			throw new AuthServiceException(ERROR_ALREADY_REGISTERED);
		}

		if(rawPass.length() == 0) {
			throw new AuthServiceException(ERROR_INVALID_PASSWORD);
		}
		
		if(fullName.length() == 0) {
			throw new AuthServiceException(ERROR_INVALID_REAL_NAME);
		}

		storage.setValue(NickAuthPlugin.NAME, nick, KEY_PASS, hashPassword(rawPass));
		storage.setValue(NickAuthPlugin.NAME, nick, KEY_NAME, fullName);

		return true;
	}

	public synchronized boolean isRegistered(String nick) {
		String storedHashPass = storage.getValue(NickAuthPlugin.NAME, nick, KEY_PASS);
		return (!storedHashPass.equals(""));
	}
	
	public synchronized String getFullName(String nick) {
		return storage.getValue(NickAuthPlugin.NAME, nick, KEY_NAME);
	}
	
	public synchronized boolean isAuthenticated(String nick) {
		return authenticatedNicks.contains(nick);
	}
	
	public synchronized void removeNickAuth(String nick) {	
		authenticatedNicks.remove(nick);
	}
	
	private String hashPassword(String rawPass) {
		MessageDigest d;
		try {
			d = MessageDigest.getInstance("MD5");
			return new String(d.digest(rawPass.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			return rawPass; //This won't happen. IRC isn't secure anyway.
		}
		
	}

}
