package nickauth;

import ircserver.Client;
import ircserver.IRCEvent;
import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;
import ircserver.Message;
import ircserver.plugin.AbstractPlugin;
import ircserver.plugin.InternalMessageFactory;
import ircserver.plugin.InternalMessageFactoryFactory;
import ircserver.plugin.PersistentStorage;
import ircserver.plugin.PluginDescriptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NickAuthPlugin extends AbstractPlugin {

	public static final String NAME = "nickauth";
	private static final String VERSION = "1.0";
	
	private static final String COMMAND_AUTH = "nick-auth";
	private static final String COMMAND_REGISTER = "nick-register";
	private static final String COMMAND_LOOKUP = "nick-lookup";
	
	private static final String RESPONSE_MALFORMED = "See !help " + NAME;
	private static final String RESPONSE_REGISTER_SUCCESS_S_S = "Registered %s to %s"; //Nick to full name.
	private static final String RESPONSE_REGISTER_FAILED_S = "Registration failed: %s";
	private static final String RESPONSE_NOT_REGISTERED_S = "Nick %s is not registered with auth service";
	private static final String RESPONSE_ISAUTHED_S = "Nick %s is authenticated.";
	private static final String RESPONSE_NOTHAUTHED_S = "Nick %s is not authenticated.";
	private static final String RESPONSE_AUTHFAILED_S = "Authentication failed: %s";
	private static final String RESPONSE_NICK_REGISTERED_S = "This nick is registered to %s use !nick-auth to authenticate";
	
	private static final String REASON_WRONGPASS = "Incorrect password";
	
	private static final List<String> COMMANDS = Arrays.asList(
			COMMAND_AUTH,
			COMMAND_REGISTER,
			COMMAND_LOOKUP
			);
	
	private static final List<String> HELP = Arrays.asList(
			"nick-auth <password> :: Authenticate as current nick with password",
			"nick-register <password> <real-name> :: Register current nick with password and real name",
			"nick-lookup <nick> :: Check if given nick is authenticated"
			);
	
	private InternalMessageFactoryFactory factory;
	private AuthService authService;
	private AuthServer authServer;
	
	public NickAuthPlugin(){
		super(NAME, VERSION, COMMANDS, HELP);
	}

	@Override
	public void init(PersistentStorage storage, PluginDescriptor... ps) {
		authService = new AuthService(storage);
		authServer = new AuthServer(authService);
		new Thread(authServer).start();
		factory = new InternalMessageFactoryFactory(NAME);
	}
	
	@Override
	public List<InternalMessage> response(InternalIRCMessage m) {
		List<InternalMessage> responses = new ArrayList<>();
		Message msg = m.getMessage();
		
		Client client = m.getClient();
		
		InternalMessageFactory factory = this.factory.build(client);
		
		if(msg.params.length < 3) {
			responses.add(factory.newPrivateMessage(RESPONSE_MALFORMED));
		} else {
			String command = msg.params[1].substring(2);
			
			String rawPass;
			
			switch(command){
			case COMMAND_REGISTER:
				if(msg.params.length < 4) {
					responses.add(factory.newPrivateMessage(RESPONSE_MALFORMED));
					return responses;
				}
				
				rawPass = msg.params[2];
				StringBuilder nameBuilder = new StringBuilder(msg.params.length - 2);
				
				nameBuilder.append(msg.params[3]);
				for (int i = 4; i < msg.params.length; i++) {
					nameBuilder.append(" ");
					nameBuilder.append(msg.params[i]);
				}
				
				String fullName = nameBuilder.toString();
				
				try{
					authService.register(client.getNick(), rawPass, fullName);
					responses.add(factory.newPrivateMessage(String.format(RESPONSE_REGISTER_SUCCESS_S_S, client.getNick(), fullName)));
				} catch (AuthServiceException e) {
					responses.add(factory.newPrivateMessage(String.format(RESPONSE_REGISTER_FAILED_S, e.getMessage())));
				}
				break;
			case COMMAND_AUTH:
				rawPass = msg.params[2];
				
				try {
					if(authService.authenticate(client.getNick(), rawPass)){
						responses.add(factory.newPrivateMessage("Authenticated as " + client.getNick()));
					} else {
						responses.add(factory.newPrivateMessage(String.format(RESPONSE_AUTHFAILED_S, REASON_WRONGPASS)));
					}
				} catch (AuthServiceException e) {
					responses.add(factory.newPrivateMessage(String.format(RESPONSE_AUTHFAILED_S, e.getMessage())));
				}
				break;
			case COMMAND_LOOKUP:
				String nick = msg.params[2];
				
				if(!authService.isRegistered(nick)) {
					responses.add(factory.newPrivateMessage(String.format(RESPONSE_NOT_REGISTERED_S, nick)));
				} else {
					if (authService.isAuthenticated(nick)) {
						responses.add(factory.newPrivateMessage(String.format(RESPONSE_ISAUTHED_S, nick)));
					} else {
						responses.add(factory.newPrivateMessage(String.format(RESPONSE_NOTHAUTHED_S, nick)));
					}
				}
			}
		}
		return responses;
	}

	@Override
	public List<InternalMessage> handleEvent(IRCEvent event) {
		List<InternalMessage> responses = new ArrayList<>();
		Client client = event.getClient();
		
		InternalMessageFactory factory = this.factory.build(client);
		
		switch(event.getType()){
		case USER_JOIN:
		case USER_NICK_CHANGED:
			if(authService.isRegistered(client.getNick())) {
				String fullName = authService.getFullName(client.getNick());
				responses.add(factory.newPrivateMessage(String.format(RESPONSE_NICK_REGISTERED_S, fullName)));
			}
			break;
		case USER_QUIT:
			if(authService.isAuthenticated(client.getNick())) {
				authService.removeNickAuth(client.getNick());
			}
			break;
		default:
			break;
		}
		
		return responses;
	}
	
	@Override
	public PluginDescriptor getPluginDescriptor() {
		return new PluginDescriptor(authServer.getPortNum(), NAME, VERSION, COMMANDS, HELP);
	}
	
}
