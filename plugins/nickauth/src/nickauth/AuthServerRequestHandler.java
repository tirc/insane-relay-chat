package nickauth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class AuthServerRequestHandler implements Runnable {

	
	
	private Socket client;
	private AuthService authService;

	public AuthServerRequestHandler(Socket client, AuthService authService) {
		this.client = client;
		this.authService = authService;
	}

	private void handleCommand(String line) {
		String[] splits = line.split("\\s+");
		if (splits.length < 2) {
			respond("ERROR Not enough parameters");
			return;
		}

		String command = splits[0];

		if (command.equals("ISAUTHED")) {
			if (authService.isAuthenticated(splits[1])) {
				respond("AUTHED " + splits[1]);
			} else {
				respond("NOTAUTHED " + splits[1]);
			}
		} else {
			respond("UNKNOWNCOMMAND");
		}
	}

	private void respond(String message) {
		message += "\n";

		try {
			client.getOutputStream().write(message.getBytes());
		} catch (IOException e) {
		}
	}

	@Override
	public void run() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(
					client.getInputStream()));
			String line;
			while ((line = br.readLine()) != null) {
				handleCommand(line);
			}
		} catch (IOException ignored) {
		}
	}

}
