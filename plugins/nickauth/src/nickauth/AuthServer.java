package nickauth;

import java.io.IOException;
import java.net.ServerSocket;

public class AuthServer implements Runnable {

	private AuthService authService;
	private ServerSocket socket;

	public AuthServer(AuthService authService) {
		this.authService = authService;
		try {
			socket = new ServerSocket(0);
		} catch (IOException e) {
			socket = null;
		}
	}

	public int getPortNum() {
		return (socket == null) ? -1 : socket.getLocalPort();
	}

	@Override
	public void run() {
		while(true){
			try{
				AuthServerRequestHandler handler = new AuthServerRequestHandler(socket.accept(), authService);
				new Thread(handler).start();
			} catch (IOException e) {}
		}
	}
}
