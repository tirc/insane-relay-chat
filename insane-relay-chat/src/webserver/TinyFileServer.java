package webserver;

public interface TinyFileServer {

    public static final byte[] FORBIDDEN = "HTTP/1.0 403 Access Forbidden\n\n".getBytes();
    public static final byte[] ERROR_404 = "HTTP/1.0 404 ERROR\n\n".getBytes();
	
	public byte[] forFilePath(String path);
	
}
