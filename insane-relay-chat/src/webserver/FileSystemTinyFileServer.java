package webserver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class FileSystemTinyFileServer implements TinyFileServer {

	private File directory;
	
    public static String joinPathElements(char sep, String... elements){
        return joinpathElements( sep, elements );
    }
    
    private static String joinpathElements(char sep, String[] elements){
        StringBuilder sb = new StringBuilder();

        for(String s : elements){
            if(s.charAt( 0 ) != sep)
                sb.append( sep );
            sb.append( s );
        }
        return sb.toString();
    }
	
	public FileSystemTinyFileServer(File directory){
		this.directory = directory;
	}
	
	@Override
	public byte[] forFilePath(String path) {
		path = joinPathElements('/', directory.getAbsolutePath(), path);
		
		File f = new File(path);
		if(!f.exists())
			return ERROR_404;
		if(f.isDirectory())
			return FORBIDDEN;
		byte[] ret = new byte[(int) f.length()];
		FileInputStream fis;
		try {
			fis = new FileInputStream(f);
			fis.read(ret);
			fis.close();
			return ret;
		} catch (IOException e) {
			return ERROR_404;
		}
		
	}

}
