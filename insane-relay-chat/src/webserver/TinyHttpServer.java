package webserver;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;


public class TinyHttpServer extends Thread
{
    private ServerSocket s;
    final TinyFileServer tfs;
	
	public static void main(String[] args){
		try{
			int port = Integer.parseInt(args[0]);
			TinyHttpServer s = new TinyHttpServer(new FileSystemTinyFileServer(new File("/home/student/n/nfb11131/web_test")), port);
			new Thread(s).start();
		} catch(NumberFormatException | NullPointerException | ArrayIndexOutOfBoundsException e){
			System.out.println("Please provide a valid port number to use. (>1024)");
		} catch(IOException e){
			System.out.println("An unexpected exception has occured. Please try again.");
			System.out.println(e.getMessage());
		}
	}

    public TinyHttpServer(TinyFileServer tfs, int port) throws IOException{
    	this.tfs = tfs;
    	s = new ServerSocket(port);
    }
    
    public int getPort(){
    	return s.getLocalPort();
    }

    @Override
    public void run()
    {
        while(true){
            try
            {
                new TinyHttpRequestHandler(s.accept()).start();
            }
            catch( IOException e )
            {
                e.printStackTrace();
            }
        }
    }

    private class TinyHttpRequestHandler extends Thread{

        Socket s;
        
        public TinyHttpRequestHandler(Socket s){
            this.s = s;
        }

        @Override
        public void run(){
            
            
            String request;
            String path;
            
            BufferedReader requestReader = null;
            DataOutputStream responseWriter = null;
            
            try
            {
                requestReader = new BufferedReader(new InputStreamReader(s.getInputStream()));
                responseWriter = new DataOutputStream( s.getOutputStream() );
                

                
                while(((request = requestReader.readLine()) != null) && (request.length() > 0)){
                    if(request.startsWith( "GET" )){
                        path = (request.split( " " ))[1];
                        byte[] response = tfs.forFilePath(path);

                        responseWriter.writeBytes("HTTP/1.0 200 OK\nContent-Length:"+response.length+"\n\n");
                        responseWriter.write(response);
                        responseWriter.flush();
                    }
                }
                responseWriter.close();
            }
            catch( IOException e )
            {
                e.printStackTrace();
            }
            finally
            {
                    try
                    {
                        if(requestReader != null)
                            requestReader.close();
                        if(responseWriter != null)
                            responseWriter.close();
                    }
                    catch( IOException e )
                    {
                        e.printStackTrace();
                    }
            }
        }

    }
}
