package ircserver;

public class IRCNumerics {

	/*
	 *	The server sends Replies 001 to 004 to a user upon
	 *	successful registration.
	 */
    public static final int RPL_WELCOME = 001; //Welcome to the Internet Relay Network <nick>!<user>@<host>
    public static final int RPL_YOURHOST = 002; //Your host is <servername>, running version <ver>
    public static final int RPL_CREATED = 003; //This server was created <date>
    public static final int RPL_MYINFO = 004; //<servername> <version> <available user modes> <available channel modes>"
    
    /*
     *	These replies are used with the AWAY command (if
     *	allowed).  RPL_AWAY is sent to any client sending a
     *	PRIVMSG to a client which is away.  RPL_AWAY is only
     *	sent by the server to which the client is connected.
     *	Replies RPL_UNAWAY and RPL_NOWAWAY are sent when the
     *	client removes and sets an AWAY message.
     */
     public static final int RPL_AWAY = 301; //<nick> :<away message>
     public static final int RPL_UNAWAY = 305; //:You are no longer marked as being away
     public static final int RPL_NOAWAY = 306; //:You have been marked as being away

     public static final int RPL_WHOISUSER = 311; //<nick> <user> <host> * :<real name>
     public static final int RPL_WHOISOPERATOR = 313; //<nick> :is an IRC operator
     public static final int RPL_ENDOFWHOIS = 318; //<nick> :End of WHOIS list
     public static final int RPL_WHOISCHANNELS = 319; //<nick> :*( ( "@" / "+" ) <channel> " " )
    
     public static final int RPL_LISTSTART = 321; // Channels :Users Name
     public static final int RPL_LIST = 322; //<channel> <# visible> :<topic> 
     public static final int RPL_LISTEND = 323; //:End of LIST
     
	/*    Replies RPL_LIST, RPL_LISTEND mark the actual replies
	 *    with data and end of the server's response to a LIST
	 *    command.  If there are no channels available to return,
	 *    only the end reply MUST be sent.
	 */
     
     public static final int RPL_TOPIC = 332; //<channel> :<topic>
     
     public static final int RPL_INVITING = 341;
     
     public static final int RPL_NAMREPLY = 353;
     //"( "=" / "*" / "@" ) <channel>
     //:[ "@" / "+" ] <nick> *( " " [ "@" / "+" ] <nick> )
     // "@" is used for secret channels, "*" for private
     // channels, and "=" for others (public channels).
     
     public static final int RPL_ENDOFNAMES = 366;
     //- To reply to a NAMES message, a reply pair consisting
     //  of RPL_NAMREPLY and RPL_ENDOFNAMES is sent by the
     //  server back to the client.  If there is no channel
     //  found as in the query, then only RPL_ENDOFNAMES is
     
     public static final int ERR_NOSUCHNICK = 401;
     // Used when a PRIVMSG tries to contact a non-existent user
     
     public static final int ERR_NOSUCHCHANNEL = 403;
     //Returned when a PRIVMSG attempts to send a message to a non-existent channel
     
     public static final int ERR_CANNOTSENDTOCHAN = 404;
     
     public static final int ERR_NICKNAMEINUSE = 433; //<nick> :Nickname is already in use
     //Returned when a NICK message is processed that results in an attempt to change to a currently existing nickname.
     
     public static final int ERR_USERNOTONCHANNEL = 442;
     public static final int ERR_USERONCHANNEL = 443;
     
     public static final int ERR_NEEDMOREPARAMS = 461;
     
     public static final int ERR_CHANOPRIVSNEEDED = 482;
     
     public static String forNumeric(int numeric){
    	 return String.format("%03d", numeric);
     }
}