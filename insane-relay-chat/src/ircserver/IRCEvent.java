package ircserver;

public class IRCEvent {

	public enum EventType {
		SERVER_INITIALISED,
		USER_JOIN,
		USER_QUIT,
		USER_JOIN_CHANNEL, 
		USER_NICK_CHANGED, 
		CHANNEL_CREATED
	}
	
	private final EventType type;
	private final Client client;
	private final String[] params;
	
	public IRCEvent(EventType type, Client client) {
		this.type = type;
		this.client = client;
		this.params = new String[0];
	}
	
	public IRCEvent(EventType type, Client client, String[] params) {
		this.type = type;
		this.client = client;
		this.params = params;
	}
	
	public EventType getType() {
		return type;
	}
	
	public Client getClient() {
		return client;
	}
	
	public String[] getParams() {
		return params;
	}
}
