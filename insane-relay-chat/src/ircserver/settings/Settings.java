package ircserver.settings;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.esotericsoftware.minlog.Log;
import com.google.gson.Gson;

/**
 * Settings Singleton. Designed to be initialised with GSON/JSON.
 * @author Elliot
 *
 */
public class Settings {

	private static Settings instance;
	private static Gson g = new Gson();
	
	private String pluginDir;
	private String[] orderedPlugins;
	private DbSettings dbSettings;
	
	private Settings(){}
	
	public static Settings getInstance(){
		return instance;
	}
	
	public String pluginDir(){
		return pluginDir;
	}
	
	public String[] orderedPlugins(){
		return orderedPlugins;
	}
	
	public DbSettings dbSettings(){
		return dbSettings;
	}
	
	public static void loadSettings(String filename){
		try {
			FileReader f = new FileReader(filename);
			Settings s = g.fromJson(f, Settings.class);
			if(s != null){
				if(s.orderedPlugins == null)
					s.orderedPlugins = new String[0];
				if(s.pluginDir == null)
					s.pluginDir = "";
				instance = s;
			}
		} catch (FileNotFoundException e) {
			Log.warn(String.format("Settings file not found: %s", filename));
			instance = new Settings();
			instance.pluginDir = "";
			instance.orderedPlugins = new String[0];
		}
		
	}
	
	public static void main(String[] args){
		Log.TRACE();
		Settings.loadSettings("./irc.conf");
		Log.trace(Settings.getInstance().pluginDir());
		DbSettings dbSettings = Settings.getInstance().dbSettings();
		Log.trace(dbSettings.host());
		Log.trace(dbSettings.dbName());
		Log.trace(dbSettings.username());
		Log.trace(dbSettings.password());
		Log.trace(Integer.toString(dbSettings.port()));
		
		for(String s : Settings.getInstance().orderedPlugins)
			Log.trace(s);
		
	}
}
