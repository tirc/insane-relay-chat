package ircserver.settings;

public class DbSettings {

	private String host;
	private String dbName;
	private String username;
	private String password;
	private int port;

	public String host() {
		return host;
	}

	public String dbName() {
		return dbName;
	}

	public String username() {
		return username;
	}

	public String password() {
		return password;
	}

	public int port() {
		return port;
	}

}
