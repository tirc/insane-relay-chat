package ircserver;

public class StringUtils {

	public static String asString(String... strings){
		if(strings.length == 0)
			return "";
		
		StringBuilder sb = new StringBuilder();
		sb.append(strings[0]);
		
		for(int i = 1; i < strings.length; i++){
			sb.append(" ");
			sb.append(strings[i]);
		}
		
		return sb.toString();
	}
	
	public static String longMessage(String... strings) {
		return ":" + asString(strings);
	}
}
