package ircserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.esotericsoftware.minlog.Log;

public class RequestHandler implements Runnable {

	private Client client;
	private IRCProtocol protocol;
	
	public RequestHandler(Server server, Client client) {
		this.client = client;
		protocol = new IRCProtocol(this, server, client);
	}

	public Client getClient() {
		return client;
	}
	
	private void processMessage(Message msg) {
		switch (msg.command.toUpperCase()) {
		case IRCReservedWords.PING:
			protocol.handlePing();
			break;
		case IRCReservedWords.USER:
			protocol.handleUser(msg);
			break;
		case IRCReservedWords.NICK:
			protocol.handleNick(msg);
			break;
		case IRCReservedWords.JOIN:
			protocol.handleJoin(msg);
			break;
		case IRCReservedWords.PRIVMSG:
			protocol.handlePrivMsg(msg);
			break;
		case IRCReservedWords.PART:
			protocol.handlePart(msg);
			break;
		case IRCReservedWords.NAMES:
		case IRCReservedWords.WHO:
			protocol.handleNames(msg);
			break;
		case IRCReservedWords.LIST:
			protocol.handleList();
			break;
		case IRCReservedWords.TOPIC:
			protocol.handleTopic(msg);
			break;
		case IRCReservedWords.KICK:
			protocol.handleKick(msg);
			break;
		case IRCReservedWords.AWAY:
			protocol.handleAway(msg);
			break;
		case IRCReservedWords.WHOIS:
			protocol.handleWhois(msg);
			break;
		case IRCReservedWords.INVITE:
			protocol.handleInvite(msg);
			break;
		case IRCReservedWords.QUIT:
			protocol.handleQuit(true);
			break;
		}
	}
	
	public void sendMessage(Message msg) {
		//Log.debug("Client" + client.getNum() + " <- " + msg);
		try {
			client.sendMessage(msg);
		} catch (IOException e) {
			Log.warn("IO error sending message to client " + client.getNum() + ": " + e.getMessage());
		}
	}
	
	@Override
	public void run() {
		Log.debug("Started RequestHandler for client " + client.getNum());
		
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(client.getSocket().getInputStream()));
			String line;
			while((line = br.readLine()) != null){
				Log.debug("Client" + client.getNum() + " -> " + ": " + line);
				processMessage(new Message(line));
			}
		} catch (IOException e) {
			Log.error("IO error with client " + client.getNum() + ": " + e.getMessage());
			protocol.handleQuit(false);
		}
	}
	
}
