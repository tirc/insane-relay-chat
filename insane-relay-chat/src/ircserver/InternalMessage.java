package ircserver;

public abstract class InternalMessage {

	protected final Client client;
	
	protected InternalMessage(Client client) {
		this.client = client;
	}
	
	public Client getClient() {
		return client;
	}

}
