package ircserver;

public class MessageFactory {

	public static Message serverResponse(String command, String... params) {
		return new Message(":" + Server.SERVER_NAME, command, params);
	}

	public static Message serverNumericResponse(int numeric, String... params) {
		return new Message(":" + Server.SERVER_NAME, IRCNumerics.forNumeric(numeric), params);
	}

	public static Message clientResponse(Client client, String command, String... params) {
		return new Message(client.getPrefix(), command, params);
	}
	
	public static Message rawMessage(String... strings) {
		return new Message(StringUtils.asString(strings));
	}
}
