package ircserver;



public class Message {

	public final String prefix;
	public final String command;
	public final String[] params;
	
	public Message(String prefix, String command, String... params) {
		this.prefix = prefix;
		this.command = command;
		this.params = params;
	}
	
	public Message(String message){
		
		String[] toks = message.split(" ");
		
		if(toks.length == 0){
			prefix = "";
			command = "";
			params = new String[]{};
		} else{
			int i = 0;
			if(toks[0].startsWith(":")){
				prefix = toks[0];
				i++;
			}
			else
				prefix = "";
			command = toks[i++];
			params = new String[toks.length-i];
			int j = 0;
			for(;i<toks.length; i++){
				params[j++] = toks[i];
			}
		}
	}
	
	@Override
	public String toString() {
		return String.format("%s %s %s\r\n", prefix, command, StringUtils.asString(params));
	}
	
}
