package ircserver;

public class InternalIRCMessage extends InternalMessage {

	private final Message message;
	
	public InternalIRCMessage(Client client, Message message) {
		super(client);
		this.message = message;
	}

	public Message getMessage() {
		return message;
	}
	
}
