package ircserver;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.esotericsoftware.minlog.Log;

public class Client {

	public enum GlobalMode {
		ADMIN("A"),
		AWAY("a"), ;
		
		private final String ircVal;
		private static final Map<String, GlobalMode> lookup = new HashMap<>();
		
		static {
			for (GlobalMode m : GlobalMode.values()) 
				lookup.put(m.getIRCVal(), m);
		}
		
		private GlobalMode(String ircVal) {
			this.ircVal = ircVal;
		}
		
		public String getIRCVal() {
			return ircVal;
		}
		
		public static GlobalMode get(String ircVal) {
			return lookup.get(ircVal);
		}
	}
	
	public enum ChannelUserMode {
		OP("o"),
		VOICED("v");
		
		private final String ircVal;
		private static final Map<String, ChannelUserMode> lookup = new HashMap<>();
		
		static {
			for (ChannelUserMode m : ChannelUserMode.values()) 
				lookup.put(m.getIRCVal(), m);
		}
		
		private ChannelUserMode(String ircVal) {
			this.ircVal = ircVal;
		}
		
		public String getIRCVal() {
			return ircVal;
		}
		
		public static ChannelUserMode get(String ircVal) {
			return lookup.get(ircVal);
		}
	}
	
	private Socket socket;
	private int clientNum;
	private String userName;
	private String nick;
	private String hostName;
	private String awayReason;
	private Set<GlobalMode> modes;
	private HashMap<Channel, Set<ChannelUserMode>> channelModes;

	public Client(Socket socket, int clientNum) {
		this.socket = socket;
		this.clientNum = clientNum;
		hostName = "unknown-host";
		awayReason = null;
		modes = new HashSet<>();
		channelModes = new HashMap<>();
	}
	
	public Socket getSocket() {
		return socket;
	}
	
	public int getNum() {
		return clientNum;
	}
	
	public String getNick() {
		return nick;
	}
	
	public String fullName() {
		return StringUtils.longMessage(nick, "!", userName, "@", hostName);
	}

	public void sendMessage(Message msg) throws IOException {	
		Log.debug("Client" + clientNum + " <- " + msg);
		
		byte[] msgBytes = msg.toString().getBytes();
		socket.getOutputStream().write(msgBytes);
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}
	
	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public boolean isClientIdentified() {
		return (userName != null && nick != null);
	}

	public String getPrefix() {
		return String.format(":%s!%s@%s", nick, userName, hostName);
	}

	public void setMode(GlobalMode mode) {
		modes.add(mode);
	}
	
	public void unsetMode(GlobalMode mode) {
		if (modes.contains(mode)) {
			modes.remove(mode);
		}
	}
	
	public boolean isAdmin() {
		return modes.contains(GlobalMode.ADMIN);
	}
	

	public boolean isAway() {
		return modes.contains(GlobalMode.AWAY);
	}

	public void addChannel(Channel chan) {
		channelModes.put(chan, new HashSet<ChannelUserMode>());
	}
	
	public Set<Channel> getChannels() {
		return channelModes.keySet();
	}

	public void closeConnection() {
		try {
			socket.close();
		} catch (IOException e) {
			Log.error("Failed to close socket for client" + clientNum);
		}
	}

	public void setChannelMode(Channel chan, ChannelUserMode mode) {
		if (channelModes.containsKey(chan)) {
			channelModes.get(chan).add(mode);
		}
	}
	
	public void unsetChannelMode(Channel chan, ChannelUserMode mode) {
		if (channelModes.containsKey(chan)) {
			Set<ChannelUserMode> modeSet = channelModes.get(chan);
			
			if (modeSet.contains(mode)) {
				modeSet.remove(mode);
			}
		}
	}
	
	public boolean isVoiced(Channel chan) {
		if (channelModes.containsKey(chan)) {
			return channelModes.get(chan).contains(ChannelUserMode.VOICED);
		}
		
		return false;
	}
	
	public boolean isChannelOp(Channel chan) {
		if (channelModes.containsKey(chan)) {
			return channelModes.get(chan).contains(ChannelUserMode.OP);
		}
		
		return false;
	}
	
	public void setAwayReason(String reason) {
		awayReason = reason;
	}
	
	public String getAwayReason() {
		return awayReason;
	}
} 
