package ircserver;

import ircserver.Client.ChannelUserMode;
import ircserver.Client.GlobalMode;
import ircserver.IRCEvent.EventType;
import ircserver.plugin.MongoDbPersistentStorage;
import ircserver.plugin.PersistentStorage;
import ircserver.plugin.Plugin;
import ircserver.plugin.PluginDescriptor;
import ircserver.plugin.PluginLoader;
import ircserver.plugin.builtin.HelpMessagePlugin;
import ircserver.settings.DbSettings;
import ircserver.settings.Settings;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.esotericsoftware.minlog.Log;

public class Server implements Runnable {

	private static final int IRC_DEFAULT_PORT = 6667;
	protected static final String SERVER_VERSION = "0.0.2";
	protected static final String SERVER_NAME = "InsaneRelayChat";
	private ServerSocket listenSocket;
	private boolean running;
	protected List<Client> clients;
	protected Map<String, Client> nickMap;
	protected List<Channel> channels;
	private Set<Plugin> plugins;
	private PersistentStorage storage;
	private ExecutorService threadPool = Executors.newCachedThreadPool();
	
	public Server() throws IOException {
		listenSocket = new ServerSocket(IRC_DEFAULT_PORT);
		running = true;
		clients = new ArrayList<>();
		channels = new ArrayList<>();
		nickMap = new HashMap<>();
		plugins = new HashSet<>();
		Settings.loadSettings("./irc.conf");
		
		DbSettings dbSettings = Settings.getInstance().dbSettings();
		if(dbSettings != null)
			storage = new MongoDbPersistentStorage(dbSettings.host(), dbSettings.dbName(), dbSettings.username(), dbSettings.password());
		
		loadPlugins();
		triggerEvent(EventType.SERVER_INITIALISED, null);
	}
	
	public boolean hasNick(String nick) {
		for (Plugin p : plugins) {
			PluginDescriptor descriptor = p.getPluginDescriptor();
			
			if (descriptor.pluginName.equals(nick)) {
				return true;
			}
		}
		
		return nickMap.containsKey(nick);
	}
	
	public void newNick(String nick, Client client) {
		nickMap.put(nick, client);
	}
	
	private void loadPlugins() {
		Log.trace("Loading plugins.");
		PluginLoader loader = new PluginLoader(storage);
		plugins = loader.loadPlugins();
		
		HelpMessagePlugin helpPlugin = new HelpMessagePlugin();
		helpPlugin.init(storage);
		
		for (Plugin p : plugins) {
			PluginDescriptor desc = p.getPluginDescriptor();
			helpPlugin.addMessage(desc.pluginName, desc.help);
		}
		
		plugins.add(helpPlugin);
	}
	
	
	@Override
	public void run() {
		Log.info(SERVER_NAME + " version " + SERVER_VERSION + " started");
		int clientNum = 0;
		
		while (running) {
			try {
				Socket clientSocket = listenSocket.accept();
				Client client = new Client(clientSocket, clientNum++);
				clients.add(client);
				threadPool.submit(new RequestHandler(this, client));
			} catch (IOException e) {
				Log.warn("IO error on client connect: " + e.getMessage());
			}
		}
	}

	public void swapNick(String oldNick, String nick, Client client) {
		nickMap.remove(oldNick);
		nickMap.put(nick, client);
	}

	public boolean joinChannel(String chanName, Client client) {
		Channel chan = getChannel(chanName);
		
		if (chan == null) {
			chan = createChannel(chanName);
		}
		
		client.addChannel(chan);
		chan.addClient(client);
		
		return true; // TODO: Channels could potentially be private once modes are implemented
	}
	
	public Channel createChannel(String chanName) {
		Channel chan = new Channel(chanName);
		channels.add(chan);
		triggerEvent(EventType.CHANNEL_CREATED, null, new String[]{chanName});
		
		return chan;
	}
	
	public Channel getChannel(String chanName) {
		for (Channel c : channels) {
			if (c.getName().equals(chanName))
				return c;
		}
		
		return null;
	}

	public Client getClient(String recipientNick) {
		for (Client c : clients) {
			if (c.getNick().equals(recipientNick)) {
				return c;
			}
		}
		
		return null;
	}

	public void handlePluginMessage(RequestHandler reqHandler, Message msg) {
		String command = msg.params[1].substring(2);
		
		for (Plugin p : plugins) {
			PluginDescriptor desc = p.getPluginDescriptor();
			
			if (desc.commands.contains(command)) {
				InternalIRCMessage incoming = new InternalIRCMessage(reqHandler.getClient(), msg);
				List<InternalMessage> responses = p.response(incoming);
				
				if (responses == null) {
					continue;
				}
				
				for (InternalMessage response : responses) {
					handleInternalMessage(response, reqHandler);
				}
			}
		}
		
	}

	private void handleInternalMessage(InternalMessage response, RequestHandler handler) {
		if (response instanceof InternalIRCMessage) {
			Message rawMsg = ((InternalIRCMessage)response).getMessage();
			Client c = ((InternalIRCMessage)response).getClient();
			try {
				c.sendMessage(rawMsg);
			} catch (IOException ignored) {}
		} else if (response instanceof InternalCommandMessage) {
			InternalCommandMessage cmdMsg = (InternalCommandMessage) response;
			String[] params = cmdMsg.getParams();
			
			switch (cmdMsg.getCommand()) {
			case SET_USER_MODE:
				processGlobalModeString(params[0], handler.getClient());
				break;
			case SET_CHANNEL_USER_MODE:
				processChannelUserModeString(params[1], handler.getClient(), params[0]);
				break;
			case CREATE_CHANNEL:
				String chanName = params[0];
				if (getChannel(chanName) == null) {
					createChannel(chanName);
				}
				break;
			default:
				break;
			}
		}
	}
	
	private void processGlobalModeString(String modeStr, Client client) {
		GlobalMode mode = GlobalMode.get(modeStr.substring(1));
		
		if (mode == null) {
			return;
		}
		
		if (modeStr.charAt(0) == '+') {
			client.setMode(mode);
		} else if (modeStr.charAt(0) == '-') {
			client.unsetMode(mode);
		}
	}
	
	public void processChannelUserModeString(String modeStr, Client client, String chanName) {
		Channel chan = getChannel(chanName);
		
		if (chan == null)
			return;
		
		ChannelUserMode mode = ChannelUserMode.get(modeStr.substring(1));
		
		if (modeStr.charAt(0) == '+') {
			client.setChannelMode(chan, mode);
		} else if (modeStr.charAt(0) == '-') {
			client.unsetChannelMode(chan, mode);
		}
	}
	
	public void leaveChannel(String chanName, Client client, String reason) {
		Channel chan = getChannel(chanName);
		
		if (chan == null)
			return;
		
		Set<Channel> clientChannels = client.getChannels();
		if (clientChannels.contains(chan)) {
			clientChannels.remove(chan);
			chan.removeClient(client, reason);
		}
	}

	public void removeClient(Client client) {
		clients.remove(client);
	}

	public List<Channel> getChannels() {
		return channels;
	}

	public void triggerEvent(IRCEvent.EventType type, RequestHandler handler) {
		triggerEvent(type, handler, new String[0]);
	}
	
	public void triggerEvent(IRCEvent.EventType type, RequestHandler handler, String[] params) {
		Client client = null;
		
		if (handler != null) {
			client = handler.getClient();
		}
		
		IRCEvent event = new IRCEvent(type, client, params);
		
		for (Plugin p : plugins) {
			List<InternalMessage> responses = p.handleEvent(event);
			
			if (responses != null) {
				for (InternalMessage im : responses) {
					handleInternalMessage(im, handler);
				}
			}
		}
	}
}
