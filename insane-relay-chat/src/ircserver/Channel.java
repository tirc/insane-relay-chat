package ircserver;

import ircserver.Client.ChannelUserMode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.esotericsoftware.minlog.Log;

public class Channel {

	private List<Client> clients;
	private String name;
	private String topic;
	
	public Channel(String name) {
		this.name = name;
		clients = new ArrayList<Client>();
		topic = "No topic set";
	}

	public String getName() {
		return name;
	}

	public void addClient(Client client) {
		Message msg = MessageFactory.clientResponse(client, "JOIN", name);
		clients.add(client);
		
		// By default we automatically voice users when joining a channel, this can be overridden
		// by a plugin listening for the USER_JOIN_CHANNEL event
		client.setChannelMode(this, ChannelUserMode.VOICED);
		
		sendMessage(msg);
	}
	
	public void removeClient(Client client) {
		removeClient(client, "");
	}
	
	public void removeClient(Client client, String reason) {
		if (!reason.equals("") && !reason.startsWith(":")) {
			reason = ":" + reason;
		}
		
		Message msg = MessageFactory.clientResponse(client, "PART", name, reason);
		sendMessage(msg);
		clients.remove(client);
	}
	
	public List<Client> getClients() {
		return clients;
	}

	public void sendMessage(Client source, Message msg) {
		msg = new Message(source.getPrefix(), msg.command, msg.params);		
		
		for (Client c : clients) {
			if (c.equals(source))
				continue;
			
			try {
				c.sendMessage(msg);
			} catch (IOException e) {
				Log.error("IO Error sending message: " + e.getMessage());
			}
		}
	}
	
	public void sendMessage(Message msg) {
		for (Client c : clients) {		
			try {
				c.sendMessage(msg);
			} catch (IOException e) {
				Log.error("IO Error sending message: " + e.getMessage());
			}
		}
	}

	public void clientQuit(Client client) {
		Message msg = MessageFactory.rawMessage(client.getPrefix(), "QUIT", ":User quitting");
		clients.remove(client);
		sendMessage(msg);
	}

	public int getNumClients() {
		return clients.size();
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String newTopic) {
		topic = newTopic;
	}

}
