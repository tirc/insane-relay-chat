package ircserver;

import ircserver.Client.GlobalMode;
import ircserver.IRCEvent.EventType;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.esotericsoftware.minlog.Log;

public class IRCProtocol {
	private final RequestHandler handler;
	private final Client client;
	private final Server server;

	public IRCProtocol(RequestHandler handler, Server server, Client client) {
		this.handler = handler;
		this.client = client;
		this.server = server;
	}

	public void handlePing() {
		sendMessage(MessageFactory.serverResponse(IRCReservedWords.PONG));
	}

	public void handleUser(Message msg) {
		String userName = msg.params[0];
		client.setUserName(userName);

		if (client.isClientIdentified()) {
			doWelcomeMessage();
		}
	}

	public void handleNick(Message msg) {
		if (msg.params.length == 0) {
			Log.warn("Received NICK message with no nickname");
			return;
		}

		String nick = msg.params[0];
		if (nick.startsWith(":"))
			nick = nick.substring(1);
		
		if (server.hasNick(nick)) {
			sendMessage(MessageFactory.serverNumericResponse(
					IRCNumerics.ERR_NICKNAMEINUSE, nick,
					StringUtils.longMessage("Nickname in use")));
		} else {
			if (client.isClientIdentified()) {
				String oldNick = client.getNick();
				server.swapNick(oldNick, nick, client);
				client.setNick(nick);
				
				String prefix = ":" + oldNick + "!" + client.getUserName() + "@" + client.getHostName();
				Message nickResponse = MessageFactory.rawMessage(prefix, IRCReservedWords.NICK, ":" + nick);
				sendMessage(nickResponse);
				
				for (Channel c : client.getChannels()) {
					c.sendMessage(nickResponse);
				}
				
				server.triggerEvent(EventType.USER_NICK_CHANGED, handler);
			} else {
				server.newNick(nick, client);
				client.setNick(nick);

				if (client.isClientIdentified()) {
					doWelcomeMessage();
				}
			}
		}
	}
	
	public void handleJoin(Message msg) {
		String chanName = msg.params[0]; // TODO: Standard allows multiple channels per join message
		
		if (!chanName.startsWith("#"))
			return;
		
		if (server.joinChannel(chanName, client)) {
			server.triggerEvent(EventType.USER_JOIN_CHANNEL, handler, new String[]{chanName, client.getNick()});
		}
	}

	public void handlePart(Message msg) {
		String chanName = msg.params[0];
		
		if (!chanName.startsWith("#"))
			return;
		
		server.leaveChannel(chanName, client, "");
	}
	
	private void doWelcomeMessage() {
		Log.trace("Client " + client.getNum()
				+ " is identified to server, showing welcome message");

		sendMessage(MessageFactory.serverNumericResponse(
				IRCNumerics.RPL_WELCOME,
				client.getNick(),
				StringUtils.longMessage("Welcome to InsaneRelayChat,",
						client.getNick())));
		sendMessage(MessageFactory.serverNumericResponse(
				IRCNumerics.RPL_YOURHOST, client.getNick(), StringUtils
						.longMessage("Your host is", Server.SERVER_NAME,
								"running version", Server.SERVER_VERSION)));
		sendMessage(MessageFactory.serverNumericResponse(
				IRCNumerics.RPL_CREATED, client.getNick(), "This server was created in the past"));
		sendMessage(MessageFactory.serverNumericResponse(
				IRCNumerics.RPL_MYINFO, client.getNick(), Server.SERVER_NAME,
				Server.SERVER_VERSION, "io", "i"));
		
		server.triggerEvent(EventType.USER_JOIN, handler);
	}

	private void sendMessage(Message msg) {
		handler.sendMessage(msg);
	}

	public void handlePrivMsg(Message msg) {
		String recipient = msg.params[0];
				
		if (msg.params[1].startsWith(":!")) {
			server.handlePluginMessage(handler, msg);
		} else if (recipient.startsWith("#")) {
			sendChannelMessage(recipient, msg);
		} else {
			sendUserMessage(recipient, msg);
		}
	}
	
	public void handleNames(Message msg) {
		String chanName = msg.params[0];
		
		if (!chanName.startsWith("#")) {
			chanName = "#" + chanName;
		}
		
		Channel chan = server.getChannel(chanName);
		
		if (chan == null) {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NOSUCHCHANNEL, chanName, StringUtils.longMessage("No such channel")));
			return;
		}
		
		List<Client> clients = chan.getClients();
		
		for (Client c : clients) {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_NAMREPLY, client.getNick(), "=",
											chanName, StringUtils.longMessage(c.getNick())));
		}
		sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_ENDOFNAMES, client.getNick(), chanName, StringUtils.longMessage("End of /NAMES list")));
	}
	
	public void handleList() {
		// The 321 command is technically deprecated but some clients still need it for list display to look correct
		// Don't you just love IRC ...
		sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_LISTSTART, "Channels", StringUtils.longMessage(client.getNick())));
		
		for (Channel chan : server.getChannels()) {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_LIST, client.getNick(), chan.getName(), 
							String.valueOf(chan.getNumClients()), StringUtils.longMessage(chan.getTopic())));
		}
		sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_LISTEND, client.getNick(), StringUtils.longMessage("End of /LIST")));
	}
	
	public void handleTopic(Message msg) {
		if (msg.params.length == 0) {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NEEDMOREPARAMS, "TOPIC", StringUtils.longMessage("Need channel for topic")));
			return;
		}
		
		String chanName = msg.params[0];
		
		Channel chan = server.getChannel(chanName);
		
		if (chan != null) {
			if (msg.params.length > 1) {
				// If there's anything after the channel, assume the user is trying to change the topic
				if (client.isAdmin() || client.isChannelOp(chan)) {
					String newTopic = StringUtils.asString(Arrays.copyOfRange(msg.params, 1, msg.params.length));
					
					if (newTopic.startsWith(":"))
						newTopic = newTopic.substring(1);
					
					chan.setTopic(newTopic);
				} else {
					sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_CHANOPRIVSNEEDED, chanName, StringUtils.longMessage("Need operator or admin privileges to modify channel topic")));
					return;
				}
			}
			
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_TOPIC, client.getNick(), chanName, StringUtils.longMessage(chan.getTopic())));
		} else {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NOSUCHCHANNEL, chanName, StringUtils.longMessage("No such channel ", chanName)));
		}
	}
	
	public void handleKick(Message msg) {
		if (msg.params.length < 2) {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NEEDMOREPARAMS, "KICK", StringUtils.longMessage("Need channel and user to KICK")));
			return;
		}
		
		String chanName = msg.params[0];
		Channel chan = server.getChannel(chanName);
		
		if (chan != null) {
			if (client.isAdmin() || client.isChannelOp(chan)) {
				String kickeeNick = msg.params[1];
				
				Client kickee = server.getClient(kickeeNick);
				
				if (kickee != null) {
					if (kickee.getChannels().contains(chan)) {
						String reason = "Kicked by " + client.getNick();
						
						if (msg.params.length > 2) {
							reason += " " + StringUtils.asString(Arrays.copyOfRange(msg.params, 2, msg.params.length));
						} 
						
						server.leaveChannel(chanName, kickee, reason);
						Log.info(kickeeNick + " kicked from " + chanName + " by " + client.getNick());
					} else {
						sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_USERNOTONCHANNEL, chanName, StringUtils.longMessage("User not on channel")));
					}
				} else {
					sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NOSUCHNICK, kickeeNick, StringUtils.longMessage("No such nick", kickeeNick)));
				}
			} else {
				sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_CHANOPRIVSNEEDED, chanName, StringUtils.longMessage("Need operator or admin privileges to kick a user")));
			}
		} else {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NOSUCHCHANNEL, chanName, StringUtils.longMessage("No such channel ", chanName)));
		}
	}
	
	public void handleAway(Message msg) {
		if (msg.params.length > 0) {
			String reason = StringUtils.asString(msg.params);
			
			if (reason.startsWith(":")) {
				reason = reason.substring(1);
			}
			
			client.setMode(GlobalMode.AWAY);
			client.setAwayReason(reason);
			
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_NOAWAY, client.getNick(), StringUtils.longMessage("You are marked as away")));
		} else {
			client.unsetMode(GlobalMode.AWAY);
			client.setAwayReason(null);
			
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_UNAWAY, client.getNick(), StringUtils.longMessage("You are no longer marked as away, welcome back!")));
		}
	}
	
	public void handleWhois(Message msg) {
		if (msg.params.length == 0) {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NEEDMOREPARAMS, "WHOIS", StringUtils.longMessage("Need a user to get details of")));
			return;
		}
		
		String nick = msg.params[0];
		Client user = server.getClient(nick);
		
		if (user != null) {
			String description = "User";
			
			if (user.isAdmin())
				description = "Admin";
			else if (user.isAway())
				description += " (Away: " + user.getAwayReason() + ")";
			
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_WHOISUSER, client.getNick(), nick, user.getUserName(), user.getHostName(), "*", StringUtils.longMessage(description)));
		
			boolean isOp = false;
			for (Channel chan : user.getChannels()) {
				String prefix = ":";
				
				
				if (user.isChannelOp(chan)) {
					prefix += "@";
					isOp = true;
				} else if (user.isVoiced(chan)) {
					prefix += "+";
				}
				
				sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_WHOISCHANNELS, client.getNick(), nick, prefix + chan.getName()));
			}
			
			if (isOp) {
				sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_WHOISOPERATOR, client.getNick(), nick, StringUtils.longMessage("is an IRC operator")));
			} else if (user.isAdmin()) {
				sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_WHOISOPERATOR, client.getNick(), nick, StringUtils.longMessage("is an IRC administrator")));
			}
			
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_ENDOFWHOIS, client.getNick(), nick));
		} else {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NOSUCHNICK, nick, StringUtils.longMessage("No such nick")));
		}
	}
	
	public void handleInvite(Message msg) {
		if (msg.params.length < 2) {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NEEDMOREPARAMS, "INVITE", StringUtils.longMessage("Need a user and channel to invite to")));
			return;
		}
		
		String nick = msg.params[0];
		Client user = server.getClient(nick);
		
		if (user != null) {
			String chanName = msg.params[1];
			Channel chan = server.getChannel(chanName);
			
			if (chan != null) {
				if (!user.getChannels().contains(chan)) {
					if (!user.isAway()) {
						try {
							user.sendMessage(MessageFactory.clientResponse(client, "INVITE", nick, chanName));
							sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_INVITING, client.getNick(), nick, chanName));
						} catch (IOException e) {
							Log.error("IO Error occurred when sending invite to " + chanName + " to " + nick);
						}						
					} else {
						sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_AWAY, client.getNick(), nick, StringUtils.longMessage(user.getAwayReason())));
					}
				} else {
					sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_USERONCHANNEL, chanName, StringUtils.longMessage("User already in channel")));
				}
			} else {
				sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NOSUCHCHANNEL, chanName, StringUtils.longMessage("No such channel ", chanName)));
			}
		} else {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NOSUCHNICK, nick, StringUtils.longMessage("No such nick")));
		}
	}
	
	public void handleQuit(boolean expectedQuit) {

		for (Channel chan : client.getChannels()) {
			chan.clientQuit(client);
		}
		
		if (expectedQuit)
			server.triggerEvent(EventType.USER_QUIT, handler);
		
		server.removeClient(client);
		client.closeConnection();
	}
	
	private void sendUserMessage(String recipientNick, Message msg) {
		Client recipient = server.getClient(recipientNick);
		
		if (recipient != null) {
			try {
				if (!recipient.isAway()) {
					recipient.sendMessage(new Message(client.getPrefix(), msg.command, msg.params));
				} else {
					sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.RPL_AWAY, client.getNick(), recipientNick, StringUtils.longMessage(recipient.getAwayReason())));
				}
			} catch (IOException e) {
				Log.error("IO error sending message to client " + recipient.getNum() + ": " + e.getMessage());
			}
		} else {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NOSUCHNICK, recipientNick, StringUtils.longMessage("No such nick")));
		}
	}

	private void sendChannelMessage(String chanName, Message msg) {
		Channel chan = server.getChannel(chanName);
		
		if (chan != null) {
			if (client.isVoiced(chan)) {
				chan.sendMessage(client, msg);
			} else {
				sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_CANNOTSENDTOCHAN, chanName, StringUtils.longMessage("Not voiced in channel", chanName)));
			}
		} else {
			sendMessage(MessageFactory.serverNumericResponse(IRCNumerics.ERR_NOSUCHCHANNEL, chanName, StringUtils.longMessage("No such channel")));
		}
	}

}
