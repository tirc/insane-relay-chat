package ircserver.plugin;

import ircserver.IRCEvent;
import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractPlugin implements Plugin {

	private final String name;
	private final String version;
	private final List<String> commands;
	private final List<String> help;
	protected PersistentStorage storage;
	
	public AbstractPlugin(String name, String version, List<String> commands, List<String> help) {
		this.name = name;
		this.version = version;
		this.commands = commands;
		this.help = help;
	}
	
	@Override
	public abstract List<InternalMessage> response(InternalIRCMessage m);

	@Override
	public void init(PersistentStorage storage, PluginDescriptor... ps) {
		this.storage = storage;
	}

	@Override
	public PluginDescriptor getPluginDescriptor() {
		return new PluginDescriptor(-1, name, version, commands, help);
	}

	@Override
	public List<InternalMessage> handleEvent(IRCEvent event) {
		return new ArrayList<>();
	}
	
	

}
