package ircserver.plugin;

import java.io.IOException;

import com.esotericsoftware.minlog.Log;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;

public class MongoDbPersistentStorage implements PersistentStorage {

	DB db;
	
	private static final String WARN_IGNORED = "Call to storage ignored because it was unavailable.";
	private static final String WARN_CANNOT_CONNECT = "Failed to connect to MongoDB. Storage will be unavailable.";

	//Test main to check db connection.
	public static void main(String[] args){
		Log.TRACE();
		PersistentStorage ps = new MongoDbPersistentStorage("devweb2013.cis.strath.ac.uk", "cs313g8", "cs313g8", "oowaeV9ku7oi");
		String pluginName = "myPlugin";
		String tableName = "myTable";
		String keyName = "yourKey";
		System.out.println(ps.getValue(pluginName, tableName, keyName));
		ps.setValue(pluginName, tableName, keyName, ps.getValue(pluginName, tableName, keyName) + "bob");
		System.out.println(ps.getValue(pluginName, tableName, keyName));
	}
	
	public MongoDbPersistentStorage(String host, String dbName,
			String username, String password) {
		this(host, dbName, username, password, 27017);
	}

	public MongoDbPersistentStorage(String host, String dbName,
			final String username, final String password, int port) {
		try {
			MongoClient mc = new MongoClient(host, port);
			Log.trace("MongoClient created.");
			db = mc.getDB(dbName);
			Log.trace("Db fetched.");
			
			/* This thread is used to implement a timeout mechanism due to a
			 * limitation in the mongo java wrapper.
			 */
			Thread t = new Thread(new Runnable(){

				@Override
				public void run() {
					if (!db.authenticate(username, password.toCharArray())) {
						Log.warn(WARN_CANNOT_CONNECT);
						db = null;
					}					
				}
				
			});
			
			t.start();
			
			try {
				t.join(5000);
				if(t.isAlive())
					db = null;
			} catch (InterruptedException e) {
				Log.warn(WARN_CANNOT_CONNECT);
				db = null;
			}
			
		} catch (IOException e) {
			Log.warn(String.format("MongoDB host %s was unreachable. Storage will be unavailbale", host));
			db = null;
		}
		Log.trace("MongoDbPersistentStorage constructed.");
	}

	@Override
	public void setValue(String pluginName, String tableName, String key,
			String value) {
		if(db == null){
			Log.warn(WARN_IGNORED);
			return;
		}
		DBCollection table = db.getCollection(pluginName + "." + tableName);
		BasicDBObject document = new BasicDBObject();

		document.put("key", key);
		document.put("value", value);

		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("key", key);

		DBCursor cursor = table.find(searchQuery);

		if (cursor.hasNext())
			table.update(cursor.next(), document);
		else
			table.insert(document);
	}

	@Override
	public String getValue(String pluginName, String tableName, String key) {
		if(db == null){
			Log.warn(WARN_IGNORED);
			return "";
		}
		DBCollection table = db.getCollection(pluginName + "." + tableName);
		
		BasicDBObject searchQuery = new BasicDBObject();
		searchQuery.put("key", key);
		
		DBCursor cursor = table.find(searchQuery);
		
		while(cursor.hasNext())	{
			return cursor.next().get("value").toString();
		}
		
		return "";
	}

}
