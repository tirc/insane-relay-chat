package ircserver.plugin;

import ircserver.Client;
import ircserver.IRCReservedWords;
import ircserver.InternalCommandMessage;
import ircserver.InternalCommandMessage.Command;
import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;
import ircserver.Message;

public class InternalMessageFactory {

	private final String pluginName;
	private Client client;
	private String recipient;
	
	public InternalMessageFactory(String pluginName) {
		this.pluginName = pluginName;
	}
	
	public InternalMessage newPrivateMessage(String message) {
		Message msg = new Message(getPrefix(), IRCReservedWords.PRIVMSG, recipient, message);
		return new InternalIRCMessage(client, msg);
	}

	private String getPrefix() {
		return ":" + pluginName + "!" + pluginName + "@plugin";
	}

	public void setClient(Client client) {
		this.client = client;
		this.recipient = client.getNick();
	}
	
	public void setRecipient(String recipient) {
		this.recipient = recipient;
	}

	public InternalMessage newCommandMessage(Command command, String... params) {
		return new InternalCommandMessage(client, command, params);
	}
}