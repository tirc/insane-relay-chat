package ircserver.plugin;

import java.util.List;

public class PluginDescriptor {
	
	public final int portNumber;
	public final String pluginName;
	public final String version;
	public final List<String> commands;
	public final List<String> help;
	
	public PluginDescriptor(int portNumber, String pluginName, String version, List<String> commands, List<String> help) {
		this.portNumber = portNumber;
		this.pluginName = pluginName;
		this.version = version;
		this.commands = commands;
		this.help = help;
	}
	
}
