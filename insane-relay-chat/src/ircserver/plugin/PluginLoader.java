package ircserver.plugin;

import ircserver.settings.Settings;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import com.esotericsoftware.minlog.Log;

public class PluginLoader {

	private PersistentStorage storage;
	
	
	public PluginLoader(PersistentStorage storage){
		this.storage = storage;
	}
	
	public Set<Plugin> loadPlugins(){
		Set<Plugin> plugins = new HashSet<>();
		List<PluginDescriptor> descriptors = new ArrayList<>();
		
		List<File> allJarFiles = getJarFiles(new File(Settings.getInstance().pluginDir()));
		List<File> jarFiles = new ArrayList<>();
		
		String[] orderedPlugins = Settings.getInstance().orderedPlugins();
		for (String s : orderedPlugins) {
			File f = null;
			for (File x : allJarFiles) {
				if (x.getName().equals(s)) {
					f = x;
					break;
				}
			}
			
			allJarFiles.remove(f);
			jarFiles.add(f);
		}
		
		jarFiles.addAll(allJarFiles);
		
		for(File f : jarFiles){
			Plugin p = loadPluginFromJar(f);
			if(p == null)
				continue;
			
			PluginDescriptor[] descArr = new PluginDescriptor[descriptors.size()];
			for(int i = 0; i < descriptors.size(); i++){
				descArr[i] = descriptors.get(i);
			}
			
			p.init(storage, descArr);
			
			PluginDescriptor desc = p.getPluginDescriptor();
			
			Set<String> reservedCommands = getReservedCommands(descriptors);
			
			boolean loaded = true;
			for(String command : desc.commands){
				if(reservedCommands.contains(command)){
					Log.warn(String.format("Failed to load plugin %s; Command already registered.", f.getName()));
					loaded = false;
					break;
				}
			}
			
			if(!loaded)
				continue;
			
			if(desc.portNumber != -1)
				Log.debug(String.format("%s: %d", desc.pluginName, desc.portNumber));
			
			descriptors.add(desc);
			plugins.add(p);
			Log.info(String.format("Loaded Plugin: %s v%s", desc.pluginName, desc.version));
			
		}
		
		return plugins;
	}
	
	private Set<String> getReservedCommands(List<PluginDescriptor> descriptors) {
		Set<String> reservedCommands = new HashSet<>();
		for(PluginDescriptor desc : descriptors)
			for(String command : desc.commands)
				reservedCommands.add(command);
		
		return reservedCommands;
	}

	private List<File> getJarFiles(File dir) {
		List<File> files = new LinkedList<>();

		File[] allFiles = dir.listFiles();
		if (allFiles == null)
			return files;
		
		for (File f : allFiles) {
			if (f.isDirectory()){
				files.addAll(getJarFiles(f));
			} else if (f.getName().endsWith(".jar")) {
				files.add(f);
			}
		}
		
		return files;
	}
	
	private Plugin loadPluginFromJar(File file){
		try {
			URL jarUrl = file.toURI().toURL();
			URLClassLoader cl = new URLClassLoader(new URL[]{jarUrl});
			JarFile jarFile = new JarFile(file);
			Enumeration<JarEntry> entries = jarFile.entries();
			List<Class<?>> loadedClasses = new ArrayList<>();
			
			while(entries.hasMoreElements()){
				JarEntry jarEntry = entries.nextElement();
				
				if(jarEntry.isDirectory() || !jarEntry.getName().endsWith(".class"))
					continue;
				
				String className = jarEntry.getName().substring(0, jarEntry.getName().length()-6).replace('/', '.');
				Class<?> c = cl.loadClass(className);
				loadedClasses.add(c);
			}
			jarFile.close();
			cl.close();
			
			Class<?> pluginClass = null;
			for(Class<?> loaded : loadedClasses){
				for(Class<?> iface : loaded.getInterfaces())
					if(iface.equals(Plugin.class))
						pluginClass = loaded;
			}
				
			if(pluginClass == null){
				for(Class<?> loaded : loadedClasses){
					if(loaded.getSuperclass().equals(AbstractPlugin.class))
						pluginClass = loaded;
				}
			}
			
			if(pluginClass != null){
				Object instance = pluginClass.newInstance();
				return (Plugin) instance;
			}
			
		} catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
			Log.warn(String.format("Failed to load plugin from jarfile: %s", file.getAbsolutePath()));
			Log.debug(String.format("Failed to load plugin from jarfile: %s", file.getAbsolutePath()), e);
		}
		return null;
	}
	
}
