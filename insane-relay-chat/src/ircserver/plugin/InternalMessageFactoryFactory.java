package ircserver.plugin;

import ircserver.Client;

public class InternalMessageFactoryFactory {

	String pluginName;
	
	public InternalMessageFactoryFactory(String pluginName) {
		this.pluginName = pluginName;
	}
	
	public InternalMessageFactory build(Client client) {
		InternalMessageFactory ret = new InternalMessageFactory(pluginName);
		
		if (client != null)
			ret.setClient(client);
		
		return ret;
	}
	
}
