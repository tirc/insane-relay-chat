package ircserver.plugin;

public interface PersistentStorage {

	public void setValue(String pluginName, String tableName, String key, String value);
	
	public String getValue(String pluginName, String tableName, String key);
	
}
