package ircserver.plugin.builtin;

import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;
import ircserver.Message;
import ircserver.plugin.AbstractPlugin;
import ircserver.plugin.InternalMessageFactory;
import ircserver.plugin.InternalMessageFactoryFactory;
import ircserver.plugin.PersistentStorage;
import ircserver.plugin.PluginDescriptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HelpMessagePlugin extends AbstractPlugin {

	private static final String NAME = "help";
	private static final String VERSION = "1.0";
	private static final List<String> COMMANDS = Arrays.asList("help");
	private static final List<String> HELP = Arrays.asList("Usage: help <plugin-name>");
	private Map<String, List<String>> messages;
	private InternalMessageFactoryFactory factory;
	
	public HelpMessagePlugin(){
		super(NAME, VERSION, COMMANDS, HELP);
	}
	
	@Override
	public void init(PersistentStorage p, PluginDescriptor... ps) {
		messages = new HashMap<>();
		factory = new InternalMessageFactoryFactory(NAME);
	}
	
	@Override
	public List<InternalMessage> response(InternalIRCMessage m) {
		List<InternalMessage> responses = new ArrayList<>();
		Message msg = m.getMessage();
		
		InternalMessageFactory factory = this.factory.build(m.getClient());
		
		if (msg.params.length < 3) {
			responses.add(factory.newPrivateMessage("Usage: help <plugin>"));
		} else {
			String plugin = msg.params[2];
			
			if (messages.containsKey(plugin)) {				
				responses.add(factory.newPrivateMessage("Help for plugin " + plugin));
				
				for (String helpLine : messages.get(plugin)) {
					responses.add(factory.newPrivateMessage(helpLine));
				}
			} else {				
				responses.add(factory.newPrivateMessage("Plugin " + plugin + " not found"));
			}
		}
		
		return responses;
	}

	public void addMessage(String pluginName, List<String> commands) {
		messages.put(pluginName, commands);
	}
}
