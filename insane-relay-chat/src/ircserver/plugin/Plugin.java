package ircserver.plugin;

import ircserver.IRCEvent;
import ircserver.InternalIRCMessage;
import ircserver.InternalMessage;

import java.util.List;

public interface Plugin {
	
	public List<InternalMessage> response(InternalIRCMessage m);
	
	public void init(PersistentStorage p, PluginDescriptor... ps);
	
	public PluginDescriptor getPluginDescriptor();
	
	public List<InternalMessage> handleEvent(IRCEvent event);
}
