package ircserver;

public class InternalCommandMessage extends InternalMessage {

	public enum Command {
		SET_USER_MODE,			// { mode-string (eg "+A"/"-A")
		SET_CHANNEL_USER_MODE,	// { #channel, mode-string }
		CREATE_CHANNEL, 		// { #channel }
		USER_JOIN_CHANNEL		// { #channel, user-nick }
	}
	
	private Command command;
	private String[] params;
	
	public InternalCommandMessage(Client client, Command command, String ... params) {
		super(client);
		this.command = command;
		this.params = params;
	}

	public Command getCommand() {
		return command;
	}
	
	public String[] getParams() {
		return params;
	}
}
