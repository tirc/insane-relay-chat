package ircserver;

public class IRCReservedWords {

	public static final String PRIVMSG = "PRIVMSG";
	public static final String PING = "PING";
	public static final String USER = "USER";
	public static final String PONG = "PONG";
	public static final String NICK = "NICK";
	public static final String JOIN = "JOIN";
	public static final String PART = "PART";
	public static final String NAMES = "NAMES";
	public static final String WHO = "WHO";
	public static final String LIST = "LIST";
	public static final String TOPIC = "TOPIC";
	public static final String KICK = "KICK";
	public static final String AWAY = "AWAY";
	public static final String WHOIS = "WHOIS";
	public static final String QUIT = "QUIT";
	public static final String INVITE = "INVITE";
	
}
