Insane Relay Chat
===
An IRC server written in Java with support for plugins. Written by [David Futcher](https://bitbucket.org/bobbo), [Elliot Iddon](https://bitbucket.org/eiddon) and [Craig Traynor](https://bitbucket.org/craigt1601) for [CS313](http://www.strath.ac.uk/cis/localteaching/localug/cs313/)

The repository contains [Eclipse](http://www.eclipse.org/) projects for the core server and serveral example plugins.

License
---
Released under the MIT License, described in LICENSE.
